//
//  IGCustomNavigationController.h
//  iGameApp
//
//  Created by lanou on 16/1/9.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGCustomNavigationController : UINavigationController

@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UIButton *iconButton;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIButton *groupButton;
@property (nonatomic, strong) UIImageView *image;
@property (nonatomic, strong) UIButton *downImage;
@property (nonatomic, strong) UILabel *label;
@end
