//
//  IGCustomNavigationController.m
//  iGameApp
//
//  Created by lanou on 16/1/9.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGCustomNavigationController.h"

@interface IGCustomNavigationController ()

@end

@implementation IGCustomNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
{
    if (self = [super initWithRootViewController:rootViewController]) {
        [self.navigationBar removeFromSuperview];
        // 背景
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, kScreenWidth, 44)];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_bgView];
        
        // 头像
        _iconButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _iconButton.frame = CGRectMake(7, 7, 30, 30);
        _iconButton.layer.cornerRadius = 15;
        _iconButton.layer.masksToBounds = YES;
        
        BOOL isLoading = [[NSUserDefaults standardUserDefaults] boolForKey:@"isLoading"];
        if (isLoading) {
            
            NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"userInfo"];
            [_iconButton sd_setBackgroundImageWithURL:[NSURL URLWithString:userInfo[@"user_img"]] forState:UIControlStateNormal];
        }else{
            [_iconButton setBackgroundImage:[UIImage imageNamed:@"uicon"] forState:UIControlStateNormal];
            
        }
        
        // 登陆后的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeImage:) name:@"USER" object:nil];
        // 登出通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeImage:) name:@"LOGOUT" object:nil];
        
        [_bgView addSubview:_iconButton];
        
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _backButton.frame = CGRectMake(10, 10, 25, 25);
        [_backButton setBackgroundImage:[UIImage imageNamed:@"iconfont-back"] forState:UIControlStateNormal];
        [_bgView addSubview:_backButton];
        _backButton.hidden = YES;
        [_backButton addTarget:self action:@selector(backButtonClick:) forControlEvents:(UIControlEventTouchUpInside)];

        _groupButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _groupButton.frame = CGRectMake((kScreenWidth - 100) / 2, 10, 100, 25);
        [_groupButton setTitle:@"游戏类别"forState:UIControlStateNormal];
        [_groupButton setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
        [_bgView addSubview:_groupButton];
        _groupButton.hidden = YES;
        
        _image = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth - 10) / 2, 35, 10, 10)];
        _image.backgroundColor = [UIColor clearColor];
        _image.image = [UIImage imageNamed:@"iconfont-sanjiaoxingshouhui"];
        _image.hidden = YES;
        [_bgView addSubview:_image];
        
        _downImage = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _downImage.frame = CGRectMake(_groupButton.right, 13, 15, 15);
        [_downImage setImage:[UIImage imageNamed:@"iconfont-jiantouxia"] forState:(UIControlStateNormal)];
        _downImage.hidden = YES;
        [_bgView addSubview:_downImage];
        
        _label = [[UILabel alloc] initWithFrame:CGRectMake((kScreenWidth - 120) / 2, 10, 120, 25)];
        _label.backgroundColor = [UIColor clearColor];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.textColor = [UIColor blueColor];
        _label.hidden = YES;
        [_bgView addSubview:_label];
        
        
    }
    
    return self;
    
    
}

// 接收通知 更改头像
- (void)changeImage:(NSNotification *)notification
{
    
    if ([notification.name isEqualToString:@"USER"]) {
        
        IG_UserMessage *user = [IG_UserMessage shareUserMessage];
        
        NSString *userIcon = user.dic[@"user_img"];
        
        [_iconButton sd_setBackgroundImageWithURL:[NSURL URLWithString:userIcon] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"uicon"]];
    }else{
        
        [_iconButton setBackgroundImage:[UIImage imageNamed:@"uicon"] forState:UIControlStateNormal];
        
    }
    
    
}


- (void)backButtonClick:(UIButton *)sender {
    [self popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
