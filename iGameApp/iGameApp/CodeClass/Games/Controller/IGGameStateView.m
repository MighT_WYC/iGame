//
//  IGGameStateView.m
//  iGameApp
//
//  Created by lanou on 16/1/18.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGGameStateView.h"

@interface IGGameStateView ()

@property (nonatomic, assign) NSInteger oldIndex;
@property (nonatomic, assign) NSInteger index;

@end


@implementation IGGameStateView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        _index = 0;
        self.backgroundColor = [UIColor blueColor];
        _label = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, kScreenWidth, 4)];
        _label.backgroundColor = [UIColor whiteColor];
        [self addSubview:_label];
    }
    return self;
}

- (void)addButtonWithX:(CGFloat)x titleStr:(NSString *)titleStr withTag:(NSInteger)tag withCount:(NSInteger)count
{
    _imageButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    _imageButton.frame = CGRectMake(x, 12, 10, 10);
    _imageButton.backgroundColor = [UIColor whiteColor];
    _imageButton.layer.cornerRadius = 4;
    _imageButton.layer.masksToBounds = 4;
    _imageButton.tag = 100 + tag;
    [self addSubview:_imageButton];
    
    _titleButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    _titleButton.frame = CGRectMake(x - 30, _imageButton.frame.size.height  + _imageButton.frame.origin.y + 5, 70, 15);
    [_titleButton setTitle:titleStr forState:(UIControlStateNormal)];
    _titleButton.titleLabel.font = [UIFont systemFontOfSize:12];
    _titleButton.tag = 200 + tag;
    [self addSubview:_titleButton];
    
    switch (count) {
        case 1 | 2:
            _titleButton.titleLabel.font = [UIFont systemFontOfSize:12];
            break;
        case 3 | 4:
            _titleButton.titleLabel.font = [UIFont systemFontOfSize:11];
            break;
        case 5:
            _titleButton.titleLabel.font = [UIFont systemFontOfSize:10];
            break;
        case 6:
            _titleButton.titleLabel.font = [UIFont systemFontOfSize:9];
            break;
            
        default:
            break;
    }
    
    [_imageButton addTarget:self action:@selector(sectionButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [_titleButton addTarget:self action:@selector(sectionButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
}

- (void)sectionButtonAction:(UIButton *)sender
{
    UIButton *button1 = (UIButton *)[self viewWithTag:_oldIndex+100];
    UIButton *button2 = (UIButton *)[self viewWithTag:_oldIndex+200];
    button1.userInteractionEnabled = YES;
    button2.userInteractionEnabled = YES;
    [self changeOldFrame];

    if (sender.tag < 200) {
        _index = sender.tag - 100;
    }else{
        _index = sender.tag - 200;
    }
    
    [self changeFrame];
    UIButton *button3 = (UIButton *)[self viewWithTag:_index+100];
    UIButton *button4 = (UIButton *)[self viewWithTag:_index+200];
    button3.userInteractionEnabled = NO;
    button4.userInteractionEnabled = NO;
    
    _block(_index);
    _oldIndex = _index;
}

- (void)changeFrame
{
    UIButton *button = (UIButton *)[self viewWithTag:_index + 100];
    button.backgroundColor = [UIColor redColor];
    button.layer.cornerRadius = 10;
    button.layer.masksToBounds = 10;
    CGRect tempFrame = button.frame;
    tempFrame.origin.x = button.frame.origin.x - 3;
    tempFrame.origin.y = 9;
    tempFrame.size.width = 17;
    tempFrame.size.height = 17;
    button.frame = tempFrame;
}

- (void)changeOldFrame
{
    UIButton *button = (UIButton *)[self viewWithTag:_oldIndex + 100];
    button.backgroundColor = [UIColor whiteColor];
    button.layer.cornerRadius = 4;
    button.layer.masksToBounds = 4;
    CGRect tempFrame = button.frame;
    tempFrame.origin.x = button.frame.origin.x + 3;
    tempFrame.origin.y = 12;
    tempFrame.size.width = 10;
    tempFrame.size.height = 10;
    button.frame = tempFrame;
}



@end
