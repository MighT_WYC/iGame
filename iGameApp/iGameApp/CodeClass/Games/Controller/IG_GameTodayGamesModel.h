//
//  IG_GameTodayGames.h
//  iGameApp
//
//  Created by Yf66 on 16/1/14.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IG_GameTodayGamesModel : NSObject

@property (nonatomic, strong) NSNumber *cate;
@property (nonatomic, strong) NSNumber *game_id;
@property (nonatomic, strong) NSString *game_name;
@property (nonatomic, strong) NSNumber *live_now;
@property (nonatomic, strong) NSString *match_stage;
@property (nonatomic, strong) NSString *match_title;
@property (nonatomic, strong) NSNumber *tournament_id;
@property (nonatomic, strong) NSString *tournament_name;

@end
