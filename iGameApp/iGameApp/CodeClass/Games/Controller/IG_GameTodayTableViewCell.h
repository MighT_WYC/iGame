//
//  IG_GameTodayTableViewCell.h
//  iGameApp
//
//  Created by Yf66 on 16/1/14.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IG_GameTodayGamesModel.h"
@interface IG_GameTodayTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IG_GameTodayGamesModel* model;
@end
