//
//  IG_GameBigGamesViewController.m
//  iGameApp
//
//  Created by Yf66 on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_GameBigGamesViewController.h"
#import "IGRightTabBarController.h"
#import "IG_GameGamesViewController.h"
@interface IG_GameBigGamesViewController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation IG_GameBigGamesViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    IGCustomNavigationController *navC = (IGCustomNavigationController *)self.navigationController;
    navC.iconButton.hidden = YES;
    navC.backButton.hidden = NO;
    navC.groupButton.hidden = YES;
    navC.downImage.hidden = YES;
    navC.image.hidden = YES;
    navC.label.hidden = NO;
    navC.label.text = _gameName;
    IGRightTabBarController *tabBarController = (IGRightTabBarController *)self.navigationController.parentViewController;
    tabBarController.tabBar.hidden = YES;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _dataArray = [NSMutableArray array];
    [_tableView registerNib:[UINib nibWithNibName:@"IG_GameBigGameTableViewCell" bundle:nil] forCellReuseIdentifier:@"IG_GameBigGameTableViewCell"];
    
    [self setUpData];
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _requestIndex = 0;
        [self setUpData];
    }];
    _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        _requestIndex += 10;
        [self setUpData];
    }];
    

}


//  数据请求
- (void)setUpData {
    if (_requestIndex == 0) {
        [_dataArray removeAllObjects];
    }
    NSString *newUrl = [NSString stringWithFormat:@"http://www.imbatv.cn/api_2_4_0/gettournamentbygame?game_id=%ld",_gameID];
    NSMutableDictionary *parDic = [NSMutableDictionary dictionary];
    parDic[@"num"] = @"10";
    parDic[@"start"] = [NSString stringWithFormat:@"%ld",_requestIndex];
    [IGAFNetworking requestWithUrlString:newUrl parDic:parDic metho:GET finish:^(NSDictionary *dataDic) {
        NSArray *array = dataDic[@"data"];
        if (array.count != 0) {
            for (NSDictionary *dic in array) {
                IG_GameBigGameModel *model = [[IG_GameBigGameModel alloc] init];
                [model setValuesForKeysWithDictionary:dic];
                [_dataArray addObject:model];
            }
            [_tableView.mj_header endRefreshing];
            if (array.count < 10) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                [_tableView.mj_footer endRefreshing];
            }
            [_tableView reloadData];
        } else {
            _dataArray = nil;
        }
        
    } error:^(NSError *requestError) {
        NSLog(@"请求失败");
    }];
}



#pragma mark -- tableView的协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IG_GameBigGameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"IG_GameBigGameTableViewCell" forIndexPath:indexPath];
    IG_GameBigGameModel *model = _dataArray[indexPath.row];
    cell.model = model;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return (kScreenWidth - 56) / 3 - 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    IG_GameBigGameModel *model = _dataArray[indexPath.row];
    IG_GameGamesViewController *gameVC = [[IG_GameGamesViewController alloc] init];
    gameVC.tournament_id = model.tournament_idStr;
    gameVC.name = model.tournament_name;
    [self.navigationController pushViewController:gameVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
