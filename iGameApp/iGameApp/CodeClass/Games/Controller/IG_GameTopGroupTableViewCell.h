//
//  IG_GameTopGroupTableViewCell.h
//  iGameApp
//
//  Created by Yf66 on 16/1/14.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IG_GameTopGroupTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
