//
//  IG_GameGroupViewController.h
//  iGameApp
//
//  Created by Yf66 on 16/1/14.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IG_GameGroupViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
