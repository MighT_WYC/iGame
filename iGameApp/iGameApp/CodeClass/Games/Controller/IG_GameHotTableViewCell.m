//
//  IG_GameHotTableViewCell.m
//  iGameApp
//
//  Created by Yf66 on 16/1/14.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_GameHotTableViewCell.h"

@implementation IG_GameHotTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(IG_GameHotModel *)model {
    _model = model;
   
    if ([model.team_logo_A isEqualToString:@""]) {
        _teamAImage.image = [UIImage imageNamed:@"iconfont-qm"];
    } else {
    [_teamAImage sd_setImageWithURL:[NSURL URLWithString:model.team_logo_A] placeholderImage:[UIImage imageNamed:@"bg"]];
    }
    if ([model.team_logo_A isEqualToString:@""]) {
        _teamBImage.image = [UIImage imageNamed:@"iconfont-qm"];
    } else {
        [_teamBImage sd_setImageWithURL:[NSURL URLWithString:model.team_logo_B] placeholderImage:[UIImage imageNamed:@"bg"]];
    }
    
    _nameALabel.text = model.team_name_A;
    _nameBLabel.text = model.team_name_B;
    _gameNameLabel.text = model.tournament_name;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd"];
    NSString *dateLoca = [NSString stringWithFormat:@"%@",model.matchtimeStr];
    NSTimeInterval time=[dateLoca doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    NSString *timestr = [formatter stringFromDate:detaildate];

    if (model.game_image == nil) {
        [_photBtn setTitle:timestr forState:(UIControlStateNormal)];
    } else {
        [_photBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:model.game_image] forState:(UIControlStateNormal)];
    }
    
    NSString *state = [NSString stringWithFormat:@"%@",model.match_state];
    if ([state isEqualToString:@"2"]) {
        _gameStateLabel.text = @"正在直播";
    } else if ([state isEqualToString:@"3"]) {
        _gameStateLabel.text = @"正在进行";
    } else if ([state isEqualToString:@"4"]) {
        _gameStateLabel.text = @"回看";
    } else if ([state isEqualToString:@"1"]) {
        _gameStateLabel.text = @"尚未开始";
    } else if ([state isEqualToString:@"5"]) {
        _gameStateLabel.text = @"已结束";
    }
    if ([state isEqualToString:@"1"]) {
        _scoreLabel.text = timestr;
    } else {
        _scoreLabel.text = model.score;
    }
}



@end
