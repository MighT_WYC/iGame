//
//  IG_GameDataModel.h
//  iGameApp
//
//  Created by Yf66 on 16/1/14.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IG_GameDataModel : NSObject

@property (nonatomic, strong) NSString *dateStr;
@property (nonatomic, strong) NSString *week;

@end
