//
//  IG_GameBigGameTableViewCell.h
//  iGameApp
//
//  Created by Yf66 on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IG_GameBigGameModel.h"
@interface IG_GameBigGameTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoImage;
@property (nonatomic, strong) IG_GameBigGameModel *model;
@end
