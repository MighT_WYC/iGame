//
//  IG_GameHotTableViewCell.h
//  iGameApp
//
//  Created by Yf66 on 16/1/14.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IG_GameHotModel.h"
@interface IG_GameHotTableViewCell : UITableViewCell

@property (nonatomic, strong) IG_GameHotModel *model;
@property (weak, nonatomic) IBOutlet UIImageView *teamAImage;
@property (weak, nonatomic) IBOutlet UILabel *nameALabel;
@property (weak, nonatomic) IBOutlet UILabel *gameNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *gameStateLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameBLabel;
@property (weak, nonatomic) IBOutlet UIImageView *teamBImage;
@property (weak, nonatomic) IBOutlet UIImageView *goImage;
@property (weak, nonatomic) IBOutlet UIButton *photBtn;
@end
