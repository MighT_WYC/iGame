//
//  IGGameStateView.h
//  iGameApp
//
//  Created by lanou on 16/1/18.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^Block)(NSInteger);
@interface IGGameStateView : UIView

@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIButton *imageButton;
@property (nonatomic, strong) UIButton *titleButton;
@property (nonatomic, copy) Block block;

- (void)addButtonWithX:(CGFloat)x titleStr:(NSString *)titleStr withTag:(NSInteger)tag withCount:(NSInteger)count;
- (void)sectionButtonAction:(UIButton *)sender;

@end
