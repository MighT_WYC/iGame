//
//  IG_GameHotModel.m
//  iGameApp
//
//  Created by Yf66 on 16/1/14.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_GameHotModel.h"

@implementation IG_GameHotModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"matchtime"]) {
        _matchtimeStr = [NSString stringWithFormat:@"%@",value];
    }
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

@end
