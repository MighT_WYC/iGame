//
//  IG_GameTodayTableViewCell.m
//  iGameApp
//
//  Created by Yf66 on 16/1/14.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_GameTodayTableViewCell.h"

@implementation IG_GameTodayTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(IG_GameTodayGamesModel *)model {
    _model = model;
    NSString *str = model.game_name;
    if ([model.game_name rangeOfString:@"\\n"].location != NSNotFound) {
        NSString *str1 = [str substringWithRange:NSMakeRange(0, 2)];
        NSString *str2 = [str substringWithRange:NSMakeRange(4, 2)];
        str = [NSString stringWithFormat:@"%@%@",str1,str2];
    }
    _nameLabel.text = str;
    _titleLabel.text = [NSString stringWithFormat:@"%@ %@",model.tournament_name, model.match_stage];
}

@end
