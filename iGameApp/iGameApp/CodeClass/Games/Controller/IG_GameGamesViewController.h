//
//  IG_GameGamesViewController.h
//  iGameApp
//
//  Created by Yf66 on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IG_GameGamesViewController : UIViewController

@property (nonatomic, strong) NSString *match_stage;
@property (nonatomic, strong) NSString *tournament_id;
@property (nonatomic, strong) NSString *name;
@end
