//
//  IGGamesViewController.h
//  iGameApp
//
//  Created by lanou on 16/1/9.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGCustomViewController.h"
#import "IGCustomViewController.h"
#import "IG_GameDataModel.h"
#import "IG_GameTodayGamesModel.h"
#import "IG_GameHotModel.h"
@interface IGGamesViewController : IGCustomViewController
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *collectionDataArray;
@property (nonatomic, strong) IG_GameDataModel *todayModel;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSMutableArray *todayGamesArray;
@property (nonatomic, strong) NSString *selectDate;
@property (nonatomic, assign) NSInteger requestStart;
@property (nonatomic, strong) NSMutableArray *hotGameDataArray;
@property (nonatomic, assign) NSInteger uid;
@property (nonatomic, assign) NSInteger todayIndex;
@property (nonatomic, strong) NSString  *code;
@property (nonatomic, assign) NSInteger selectIndex;
@property (nonatomic, assign) BOOL isRefresh;
@property (nonatomic, assign) BOOL isFoot;
@property (nonatomic, strong) NSArray *gameNameArray;
@end
