//
//  IG_GameHotModel.h
//  iGameApp
//
//  Created by Yf66 on 16/1/14.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IG_GameHotModel : NSObject

@property (nonatomic, strong) NSNumber *cate;
@property (nonatomic, strong) NSNumber *game_id;
@property (nonatomic, strong) NSString *game_image;
@property (nonatomic, strong) NSString *game_name;
@property (nonatomic, strong) NSNumber *match_id;
@property (nonatomic, strong) NSString *match_stage;
@property (nonatomic, strong) NSNumber *match_state;
@property (nonatomic, strong) NSString *matchtimeStr;
@property (nonatomic, strong) NSNumber *remind_me;
@property (nonatomic, strong) NSString *remind_str;
@property (nonatomic, strong) NSString *score;
@property (nonatomic, strong) NSString *team_logo_A;
@property (nonatomic, strong) NSString *team_logo_B;
@property (nonatomic, strong) NSString *team_name_A;
@property (nonatomic, strong) NSString *team_name_B;
@property (nonatomic, strong) NSNumber *tournament_id;
@property (nonatomic, strong) NSString *tournament_name;
@property (nonatomic, strong) NSNumber *vid;


@end
