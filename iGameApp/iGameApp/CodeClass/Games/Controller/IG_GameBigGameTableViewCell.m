//
//  IG_GameBigGameTableViewCell.m
//  iGameApp
//
//  Created by Yf66 on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_GameBigGameTableViewCell.h"

@implementation IG_GameBigGameTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(IG_GameBigGameModel *)model {
    _model = model;
    [_photoImage sd_setImageWithURL:[NSURL URLWithString:model.tournament_image] placeholderImage:[UIImage imageNamed:@"bg"]];
}

@end
