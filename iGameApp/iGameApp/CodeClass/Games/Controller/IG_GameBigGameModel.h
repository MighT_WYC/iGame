//
//  IG_GameBigGameModel.h
//  iGameApp
//
//  Created by Yf66 on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IG_GameBigGameModel : NSObject

@property (nonatomic, strong) NSString *tournament_idStr;
@property (nonatomic, strong) NSString *tournament_image;
@property (nonatomic, strong) NSString *tournament_name;

@end
