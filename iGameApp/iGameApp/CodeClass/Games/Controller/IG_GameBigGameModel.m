//
//  IG_GameBigGameModel.m
//  iGameApp
//
//  Created by Yf66 on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_GameBigGameModel.h"

@implementation IG_GameBigGameModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"tournament_id"]) {
        _tournament_idStr = [NSString stringWithFormat:@"%@",value];
    }
}


- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

@end
