//
//  IG_GameBigGamesViewController.h
//  iGameApp
//
//  Created by Yf66 on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IG_GameBigGameModel.h"
#import "IG_GameBigGameTableViewCell.h"

@interface IG_GameBigGamesViewController : UIViewController
@property (nonatomic, assign) NSInteger gameID;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSInteger requestIndex;
@property (nonatomic, strong) NSString *gameName;
@end
