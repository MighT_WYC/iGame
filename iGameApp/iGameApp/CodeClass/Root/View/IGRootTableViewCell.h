//
//  IGRootTableViewCell.h
//  iGameApp
//
//  Created by lanou on 16/1/9.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGRootTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *functionLabel;

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;



@end
