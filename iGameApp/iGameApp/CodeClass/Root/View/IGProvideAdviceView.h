//
//  IGProvideAdviceView.h
//  iGameApp
//
//  Created by lanou on 16/1/15.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGProvideAdviceView : UIView
@property (weak, nonatomic) IBOutlet UIButton *cancerButton;
@property (weak, nonatomic) IBOutlet UITextView *adviceTextView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@end
