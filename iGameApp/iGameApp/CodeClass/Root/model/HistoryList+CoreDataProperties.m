//
//  HistoryList+CoreDataProperties.m
//  iGameApp
//
//  Created by lanou on 16/1/20.
//  Copyright © 2016年 WYC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "HistoryList+CoreDataProperties.h"

@implementation HistoryList (CoreDataProperties)

@dynamic titleName;
@dynamic imgUrlString;
@dynamic vid;

@end
