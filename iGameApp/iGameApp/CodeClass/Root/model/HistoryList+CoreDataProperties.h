//
//  HistoryList+CoreDataProperties.h
//  iGameApp
//
//  Created by lanou on 16/1/20.
//  Copyright © 2016年 WYC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "HistoryList.h"

NS_ASSUME_NONNULL_BEGIN

@interface HistoryList (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *titleName;
@property (nullable, nonatomic, retain) NSString *imgUrlString;
@property (nullable, nonatomic, retain) NSString *vid;

@end

NS_ASSUME_NONNULL_END
