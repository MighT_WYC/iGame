//
//  IGChangePasswordViewController.m
//  iGameApp
//
//  Created by Yf66 on 16/1/21.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGChangePasswordViewController.h"

@interface IGChangePasswordViewController ()

@end

@implementation IGChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationController.navigationBar.hidden = NO;

}

- (IBAction)changeBtn:(id)sender {
    
    if ([_nTF.text isEqualToString:_oldTF.text]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"新旧密码不能相同" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
        [alert show];
    }else{
        
        if (![_nTF.text isEqualToString:_affirmTF.text]) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"两次密码输入不同" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
            [alert show];
        }else{
            
            NSMutableDictionary *parDic = [NSMutableDictionary dictionary];
            parDic[@"newPwd"] = _nTF.text;
            parDic[@"oldPwd"] = _oldTF.text;
            NSDictionary *dic = [IG_UserMessage shareUserMessage].dic;
            NSString *uid = dic[@"uid"];
            parDic[@"uid"] = uid;
            [IGAFNetworking newRequestWithUrlString:@"http://www.imbatv.cn/api_2_4_0/modifyPassword" parDic:parDic method:POST finish:^(NSDictionary *dataDic) {
                NSInteger code = [dataDic[@"code"] integerValue];
                NSString *pMessage = dataDic[@"message"];
                if (code == 200) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"修改成功" message:pMessage delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
                    _oldTF.text = nil;
                    _nTF.text = nil;
                    _affirmTF.text = nil;
                    [alert show];
                    [self dismissViewControllerAnimated:YES completion:nil];
                } else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"修改失败" message:pMessage delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
                    [alert show];
                }
            } error:^(NSError *requestError) {
                NSLog(@"请求失败");
            }];
            
            

        }
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
