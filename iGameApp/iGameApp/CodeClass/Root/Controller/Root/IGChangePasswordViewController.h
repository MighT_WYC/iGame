//
//  IGChangePasswordViewController.h
//  iGameApp
//
//  Created by Yf66 on 16/1/21.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGChangePasswordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *changeBtn;
@property (weak, nonatomic) IBOutlet UITextField *oldTF;

@property (weak, nonatomic) IBOutlet UITextField *nTF;
@property (weak, nonatomic) IBOutlet UITextField *affirmTF;
@end
