//
//  IGHistoryListTableViewController.m
//  iGameApp
//
//  Created by lanou on 16/1/20.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGHistoryListTableViewController.h"
#import "IGNewsTableViewCell1.h"
#import "IGFindRadioPlayViewController.h"


@interface IGHistoryListTableViewController ()
@property (nonatomic, strong) NSMutableArray *listArray;
@property (nonatomic, strong) UILabel *titleLabel;
@end

@implementation IGHistoryListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"IGNewsTableViewCell1" bundle:nil] forCellReuseIdentifier:@"IGNewsTableViewCell1"];
    self.tableView.rowHeight = 100;
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kScreenWidth/2 - 100, 7, 200, 30)];
    _titleLabel.text = @"浏览记录";
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigationController.navigationBar addSubview:_titleLabel];
}

- (void)viewWillAppear:(BOOL)animated
{
    _titleLabel.hidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    _listArray = [[IGHistoryList defaultIGHistoryList] searchLocationData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    _titleLabel.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
    return _listArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IGNewsTableViewCell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell1" forIndexPath:indexPath];
    HistoryList *list = _listArray[indexPath.row];
    cell.list = list;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IGFindRadioPlayViewController *radioPlayVC = [[IGFindRadioPlayViewController alloc] init];
    HistoryList *list = _listArray[indexPath.row];
    radioPlayVC.vid = list.vid;
    radioPlayVC.isFindPushHere = NO;
    [self.navigationController pushViewController:radioPlayVC animated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
