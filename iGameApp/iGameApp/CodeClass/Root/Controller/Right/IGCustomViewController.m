//
//  IGCustomViewController.m
//  iGameApp
//
//  Created by lanou on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGCustomViewController.h"
#import "IGCustomNavigationController.h"
#import "IGRightTabBarController.h"
@interface IGCustomViewController ()

@property (nonatomic, strong) IGCustomNavigationController *naviC;

@property (nonatomic, strong) UITapGestureRecognizer *tap;

@end

@implementation IGCustomViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // 头像button添加方法
    _naviC = (IGCustomNavigationController *)self.navigationController;
    [_naviC.iconButton addTarget:self action:@selector(iconButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    // 添加手势
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    
    
}

// 头像button
- (void)iconButtonClick:(UIButton *)button
{
    IGRightTabBarController *rightTBC = (IGRightTabBarController *)_naviC.parentViewController;
    
    CGRect frame = rightTBC.view.frame;
    
    if (frame.origin.x == 0) {
        
        frame.origin.x = kScreenWidth - 50;
        [self.view addGestureRecognizer:_tap];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"closeUserInteractionEnabled" object:self userInfo:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CLOSE" object:nil userInfo:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refresh" object:nil];
    }else{
        
        frame.origin.x = 0;
        [self.view removeGestureRecognizer:_tap];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openUserInteractionEnabled" object:self userInfo:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OPEN" object:nil userInfo:nil];
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        
        rightTBC.view.frame = frame;
        
    }];
}

// 手势
- (void)tapAction:(UITapGestureRecognizer *)tap
{
    IGRightTabBarController *rightTBC = (IGRightTabBarController *)_naviC.parentViewController;
    CGRect frame = rightTBC.view.frame;
    
    if (frame.origin.x == 0) {
        ;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refresh" object:nil];
    }else{
        
        frame.origin.x = 0;
        [self.view removeGestureRecognizer:_tap];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OPEN" object:nil userInfo:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"openUserInteractionEnabled" object:self userInfo:nil];
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        
        rightTBC.view.frame = frame;
        
    }];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
