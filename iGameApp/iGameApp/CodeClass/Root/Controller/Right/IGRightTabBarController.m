//
//  IGRightTabBarController.m
//  iGameApp
//
//  Created by lanou on 16/1/9.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGRightTabBarController.h"
#import "IGCustomNavigationController.h"
#import "IGCustomViewController.h"
#import "IGNewsViewController.h"
#import "IGGamesViewController.h"
#import "IGiGameViewController.h"
#import "IGFindViewController.h"
#import "IGLiveViewController.h"
@interface IGRightTabBarController ()

@end

@implementation IGRightTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self addViewController:[IGNewsViewController class] withName:@"资讯" image:[UIImage imageNamed:@"news"]];
    [self addViewController:[IGGamesViewController class] withName:@"赛事" image:[UIImage imageNamed:@"game"]];
    [self addViewController:[IGiGameViewController class] withName:@"iGame" image:[UIImage imageNamed:@"iGame"]];
    [self addViewController:[IGFindViewController class] withName:@"发现" image:[UIImage imageNamed:@"find"]];
    [self addViewController:[IGLiveViewController class] withName:@"直播" image:[UIImage imageNamed:@"live"]];

    
    self.tabBar.tintColor = [UIColor purpleColor];
    self.tabBar.barTintColor = [UIColor whiteColor];

}

// 添加子控制器
- (void)addViewController:(Class)class withName:(NSString *)name image:(UIImage *)image
{
    IGCustomViewController *viewController = [[class alloc] init];
    IGCustomNavigationController *naviC = [[IGCustomNavigationController alloc] initWithRootViewController:viewController];
    naviC.tabBarItem.title = name;
    naviC.tabBarItem.image = image;
    [self addChildViewController:naviC];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
