//
//  IGNewsTournamentViewController.m
//  iGameApp
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGNewsTournamentViewController.h"
#import "IGNewsTournamentHeader.h"
#import "IGNewsTournamentModel.h"

#import "IGNewsTableViewCell1.h"
#import "IGNewsTableViewCell2.h"
#import "IGNewsTableViewCell3.h"
#import "IGNewsTableViewCell4.h"
#import "IGNewsTableViewCell5.h"
#import "IGNewsTableViewCell6.h"

#import "IGCustomNavigationController.h"
#import "IGNewsArticleViewController.h"
#import "IGNewsVideoViewController.h"
#import "IG_GameGamesViewController.h"

@interface IGNewsTournamentViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) IGNewsTournamentHeader *headerView;

@property (nonatomic, strong) NSMutableArray *dataArr;

@property (nonatomic, strong) NSMutableDictionary *dataDic;

@property (nonatomic, strong) IGCustomNavigationController *naviC;

@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation IGNewsTournamentViewController

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self setHeaderView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self setNaviC];
    
    // 隐藏tabBar
    self.tabBarController.tabBar.hidden = YES;

    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self removeNaviC];
    
    // 显示tabBar
    self.tabBarController.tabBar.hidden = NO;

    
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor grayColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    // 注册cell
    [self registerCell];
    
    // 注册表头
    _headerView = [[UINib nibWithNibName:@"IGNewsTournamentHeaderView" bundle:nil] instantiateWithOwner:self options:nil].firstObject;
    
    // 区头区尾的高度
    _tableView.sectionHeaderHeight = 30;
    _tableView.sectionFooterHeight = 0;
    
    // 初始化字典
    _dataDic = [NSMutableDictionary dictionary];
    
    // 请求数据
    [self requestData];
    
}

// 导航栏
- (void)setNaviC
{
    _naviC = (IGCustomNavigationController *)self.navigationController;
    
    _naviC.iconButton.hidden = YES;
    _naviC.backButton.hidden = NO;
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake((kScreenWidth - 250) / 2, 9.5, 250, 25)];
    _titleLabel.text = _infoalbum_title;
    [_naviC.bgView addSubview:_titleLabel];
    
}

- (void)removeNaviC
{
    
    [_titleLabel removeFromSuperview];
    
}

// 注册cell
- (void)registerCell
{
    [_tableView registerNib:[UINib nibWithNibName:@"IGNewsTableViewCell1" bundle:nil] forCellReuseIdentifier:@"IGNewsTableViewCell1"];
    [_tableView registerNib:[UINib nibWithNibName:@"IGNewsTableViewCell2" bundle:nil] forCellReuseIdentifier:@"IGNewsTableViewCell2"];
    [_tableView registerNib:[UINib nibWithNibName:@"IGNewsTableViewCell3" bundle:nil] forCellReuseIdentifier:@"IGNewsTableViewCell3"];
    [_tableView registerNib:[UINib nibWithNibName:@"IGNewsTableViewCell4" bundle:nil] forCellReuseIdentifier:@"IGNewsTableViewCell4"];
    [_tableView registerNib:[UINib nibWithNibName:@"IGNewsTableViewCell5" bundle:nil] forCellReuseIdentifier:@"IGNewsTableViewCell5"];
    [_tableView registerNib:[UINib nibWithNibName:@"IGNewsTableViewCell6" bundle:nil] forCellReuseIdentifier:@"IGNewsTableViewCell6"];
    
}


- (void)setHeaderView
{
    _headerView.frame = CGRectMake(0, 0, kScreenWidth, 300);
    _tableView.tableHeaderView = _headerView;
}

- (void)requestData
{
    NSMutableDictionary *parDic = [NSMutableDictionary dictionary];
    parDic[@"infoalbum_id"] = _infoalbum_id;
    
    [IGAFNetworking requestWithUrlString:@"http://www.imbatv.cn/api_2_4_0/getInfoAlbum?" parDic:parDic metho:GET finish:^(NSDictionary *dataDic) {
        
        NSDictionary *dic = dataDic[@"data"];
        
        _headerView.contentLabel.text = dic[@"infoalbum_content"];
        [_headerView.tournamentImage sd_setImageWithURL:[NSURL URLWithString:dic[@"infoalbum_image"]]];
        
        NSDictionary *tournamentDic = dic[@"tournament_list"];
        IGNewsTournamentModel *tournamentModel = [[IGNewsTournamentModel alloc] init];
        [tournamentModel setValuesForKeysWithDictionary:tournamentDic];
        
        _dataDic[@"tournament_list"] = tournamentModel;
        
        NSArray *subtitlesArr = dic[@"subtitles"];
        NSDictionary *firstDic = subtitlesArr[0];
        NSArray *liveArr = firstDic[@"content"];
        NSMutableArray *liveTempArr = [NSMutableArray array];
        for (NSDictionary *oneDic in liveArr) {
            
            IGNewsTournamentModel *model = [[IGNewsTournamentModel alloc] init];
            [model setValuesForKeysWithDictionary:oneDic];
            [liveTempArr addObject:model];
            
        }
        _dataDic[@"现场采访"] = liveTempArr;
        
        
        NSDictionary *secondDic = subtitlesArr[1];
        NSArray *newsArr = secondDic[@"content"];
        NSMutableArray *newsTempArr = [NSMutableArray array];
        for (NSDictionary *oneDic in newsArr) {
            
            IGNewsTournamentModel *model = [[IGNewsTournamentModel alloc] init];
            [model setValuesForKeysWithDictionary:oneDic];
            [newsTempArr addObject:model];
            
        }
        _dataDic[@"新闻"] = newsTempArr;
        [_tableView reloadData];
        
    } error:^(NSError *requestError) {
        NSLog(@"%@",requestError);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        
        return 1;
        
    }else if (section == 1) {
        
        NSArray *liveArr = _dataDic[@"现场采访"];
        return liveArr.count;
        
    }else{
        
        NSArray *newsArr = _dataDic[@"新闻"];
        return newsArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            IGNewsTournamentModel *model = _dataDic[@"tournament_list"];
            IGNewsTableViewCell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell2"];
            cell.tournamentModel = model;
            cell.titleLabel.hidden = YES;
            return cell;

        }
            break;
        case 1:
        {
            NSArray *liveArr = _dataDic[@"现场采访"];
            IGNewsTournamentModel *model = liveArr[indexPath.row];
            NSString *model_type = model.model_type;
            NSInteger type = [model_type integerValue];
            
            if (type == 1) {
                IGNewsTableViewCell3 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell3"];
                    cell.videoImageView.hidden = NO;
                    cell.tournamentModel = model;
                    return cell;
            
                }else if (type == 3) {
                    IGNewsTableViewCell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell1"];
                    cell.videoImageView.hidden = YES;
                    cell.tournamentModel = model;
                    return cell;
            
                }else if (type == 9) {
                    IGNewsTableViewCell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell1"];
                    cell.videoImageView.hidden = NO;
                    cell.tournamentModel = model;
                    return cell;
                    
                }else if (type == 13) {
                    IGNewsTableViewCell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell2"];
                    cell.tournamentModel = model;
                    return cell;
                    
                }else if (type == 14) {
                    IGNewsTableViewCell3 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell3"];
                    cell.videoImageView.hidden = YES;
                    cell.tournamentModel = model;
                    return cell;
                }else if (type == 6) {
                    
                    IGNewsTableViewCell6 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell6"];
                    cell.tournamentModel = model;
                    return cell;
                    
                }
            
        }
            break;
        case 2:
        {
            NSArray *newsArr = _dataDic[@"新闻"];
            IGNewsTournamentModel *model = newsArr[indexPath.row];
            NSString *model_type = model.model_type;
            NSInteger type = [model_type integerValue];
            
            if (type == 1) {
                IGNewsTableViewCell3 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell3"];
                cell.videoImageView.hidden = NO;
                cell.tournamentModel = model;
                return cell;
                
            }else if (type == 3) {
                IGNewsTableViewCell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell1"];
                cell.videoImageView.hidden = YES;
                cell.tournamentModel = model;
                return cell;
                
            }else if (type == 9) {
                IGNewsTableViewCell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell1"];
                cell.videoImageView.hidden = NO;
                cell.tournamentModel = model;
                return cell;
                
            }else if (type == 13) {
                IGNewsTableViewCell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell2"];
                cell.tournamentModel = model;
                return cell;
                
            }else if (type == 14) {
                IGNewsTableViewCell3 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell3"];
                cell.videoImageView.hidden = YES;
                cell.tournamentModel = model;
                return cell;
                
            }else if (type == 6) {
                
                IGNewsTableViewCell6 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell6"];
                cell.tournamentModel = model;
                return cell;
                
            }
        }
            break;
            
        default:
            return nil;
            break;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        return 130;
        
    }else if (indexPath.section == 1) {
        
        NSArray *liveArr = _dataDic[@"现场采访"];
        IGNewsTournamentModel *model = liveArr[indexPath.row];
        NSString *model_type = model.model_type;
        NSInteger type = [model_type integerValue];
        
        switch (type) {
            case 1:
                return 220;
                break;
            case 3:
                return 100;
                break;
            case 9:
                return 100;
                break;
            case 13:
                return 130;
                break;
            case 14:
                return 230;
                break;
            default:
                return 100;
                break;
        }

    }else{
        
        NSArray *newsArr = _dataDic[@"新闻"];
        IGNewsTournamentModel *model = newsArr[indexPath.row];
        NSString *model_type = model.model_type;
        NSInteger type = [model_type integerValue];
        switch (type) {
            case 1:
                return 220;
                break;
            case 3:
                return 100;
                break;
            case 6:
                return 130;
                break;
            case 9:
                return 100;
                break;
            case 13:
                return 130;
                break;
            case 14:
                return 230;
                break;
            default:
                return 100;
                break;
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        
        return nil;
        
    }else if (section == 1) {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 25)];
        label.text = @"现场采访";
        label.font = [UIFont systemFontOfSize:14];
        label.textAlignment = NSTextAlignmentCenter;
        return label;
        
    }else{
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 25)];
        label.text = @"新闻";
        label.font = [UIFont systemFontOfSize:14];
        label.textAlignment = NSTextAlignmentCenter;
        return label;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        
        IGNewsTournamentModel *model = _dataDic[@"tournament_list"];
        IG_GameGamesViewController *gameVC = [[IG_GameGamesViewController alloc] init];
        gameVC.tournament_id = model.tournament_id;
        gameVC.title = model.title;
        [self.navigationController pushViewController:gameVC animated:YES];
        
        
    }
    
    if (indexPath.section == 1) {
        
        IGNewsTournamentModel *model = _dataDic[@"现场采访"][indexPath.row];
        
        if (model.article_id != nil) {
            
            IGNewsArticleViewController *articleVC = [[IGNewsArticleViewController alloc] initWithNibName:@"IGNewsArticleViewController" bundle:nil];
            articleVC.article_id = model.article_id;
            [self.navigationController pushViewController:articleVC animated:YES];
            
        }else{
            
            if ([model.model_type isEqualToString:@"6"]) {
                
                IGNewsVideoViewController *videoVC = [[IGNewsVideoViewController alloc] initWithNibName:@"IGNewsVideoViewController" bundle:nil];
                
                NSArray *vidArr = [model.vid componentsSeparatedByString:@","];
                
                videoVC.vid = vidArr[0];
                [self.navigationController pushViewController:videoVC animated:YES];

                
            }else{
            
                IGNewsVideoViewController *videoVC = [[IGNewsVideoViewController alloc] initWithNibName:@"IGNewsVideoViewController" bundle:nil];
                videoVC.vid = model.vid;
                [self.navigationController pushViewController:videoVC animated:YES];
            }
        }
        
    }
    if (indexPath.section == 2) {
        
        IGNewsTournamentModel *model = _dataDic[@"新闻"][indexPath.row];
        
        if (model.article_id != nil) {
            
            IGNewsArticleViewController *articleVC = [[IGNewsArticleViewController alloc] initWithNibName:@"IGNewsArticleViewController" bundle:nil];
            articleVC.article_id = model.article_id;
            [self.navigationController pushViewController:articleVC animated:YES];
            
        }else{
            
            if ([model.model_type isEqualToString:@"6"]) {
                
                IGNewsVideoViewController *videoVC = [[IGNewsVideoViewController alloc] initWithNibName:@"IGNewsVideoViewController" bundle:nil];
                
                NSArray *vidArr = [model.vid componentsSeparatedByString:@","];
                
                videoVC.vid = vidArr[0];
                [self.navigationController pushViewController:videoVC animated:YES];
                
                
            }else{
                
                IGNewsVideoViewController *videoVC = [[IGNewsVideoViewController alloc] initWithNibName:@"IGNewsVideoViewController" bundle:nil];
                videoVC.vid = model.vid;
                [self.navigationController pushViewController:videoVC animated:YES];
            }

        }
        
        
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
