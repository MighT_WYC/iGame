//
//  IGNewsParentViewController.h
//  iGameApp
//
//  Created by lanou on 16/1/14.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGNewsTableViewCell1.h"
#import "IGNewsTableViewCell2.h"
#import "IGNewsTableViewCell3.h"
#import "IGNewsTableViewCell4.h"
#import "IGNewsTableViewCell5.h"
#import "IGNewsTableViewCell6.h"
#import "IGNewsListModel.h"

typedef void(^PushBlock)(NSString *infomationId, NSString *infomationTitle, NSInteger model_type);


@interface IGNewsParentViewController : UIViewController


@property (nonatomic, assign) NSInteger requestIndex;

@property (nonatomic, strong) NSString *sub_id;

@property (nonatomic, copy) PushBlock pushBlock;


@end
