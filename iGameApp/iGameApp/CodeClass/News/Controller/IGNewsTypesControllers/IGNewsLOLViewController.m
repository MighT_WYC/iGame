//
//  IGNewsLOLViewController.m
//  iGameApp
//
//  Created by lanou on 16/1/14.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGNewsLOLViewController.h"

@interface IGNewsLOLViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataArr;

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;

@end

@implementation IGNewsLOLViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _activityIndicatorView.frame = CGRectMake((kScreenWidth - 50) / 2, 20, 50, 50);
    [self.view addSubview:_activityIndicatorView];
    [_activityIndicatorView startAnimating];

    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 100;
    
    // 注册cell
    [self registerCell];
    
    // 初始化数组
    _dataArr = [NSMutableArray array];
    
    // 请求数据
    [self requestData];
    
    // 刷新数据
    self.requestIndex = 0;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.requestIndex = 0;
        [self requestData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.requestIndex += 10;
        [self requestData];
        
    }];
}

// 注册cell
- (void)registerCell
{
    [_tableView registerNib:[UINib nibWithNibName:@"IGNewsTableViewCell1" bundle:nil] forCellReuseIdentifier:@"IGNewsTableViewCell1"];
    [_tableView registerNib:[UINib nibWithNibName:@"IGNewsTableViewCell2" bundle:nil] forCellReuseIdentifier:@"IGNewsTableViewCell2"];
    [_tableView registerNib:[UINib nibWithNibName:@"IGNewsTableViewCell3" bundle:nil] forCellReuseIdentifier:@"IGNewsTableViewCell3"];
    [_tableView registerNib:[UINib nibWithNibName:@"IGNewsTableViewCell4" bundle:nil] forCellReuseIdentifier:@"IGNewsTableViewCell4"];
    [_tableView registerNib:[UINib nibWithNibName:@"IGNewsTableViewCell5" bundle:nil] forCellReuseIdentifier:@"IGNewsTableViewCell5"];
    [_tableView registerNib:[UINib nibWithNibName:@"IGNewsTableViewCell6" bundle:nil] forCellReuseIdentifier:@"IGNewsTableViewCell6"];
    
    
}


// 请求数据
- (void)requestData
{
    
    IG_UserMessage *user = [IG_UserMessage shareUserMessage];
    NSDictionary *infoDic = user.dic[@"data"];
    NSString *uid = infoDic[@"uid"];
    
    NSMutableDictionary *parDic = [NSMutableDictionary dictionary];
    parDic[@"num"] = @"10";
    parDic[@"start"] = [NSString stringWithFormat:@"%ld",self.requestIndex];
    parDic[@"sub_id"] = @"7";
    parDic[@"uid"] = uid;
    
    [IGAFNetworking requestWithUrlString:@"http://www.imbatv.cn/api_2_4_0/getinfolists?" parDic:parDic metho:GET finish:^(NSDictionary *dataDic) {
        
        NSArray *dataArr = dataDic[@"data"];
        
        if (self.requestIndex == 0) {
            [_dataArr removeAllObjects];
        }
        
        for (NSDictionary *oneDic in dataArr) {
            IGNewsListModel *model = [[IGNewsListModel alloc] init];
            [model setValuesForKeysWithDictionary:oneDic];
            [_dataArr addObject:model];
        }
        
        [_activityIndicatorView stopAnimating];
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [_tableView reloadData];
        
    } error:^(NSError *requestError) {
        NSLog(@"%@",requestError);
    }];
}



#pragma mark -- dataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IGNewsListModel *model = _dataArr[indexPath.row];
    
    NSString *model_type = model.model_type;
    NSInteger type = [model_type integerValue];
    
    if (type == 1) {
        IGNewsTableViewCell3 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell3"];
        cell.videoImageView.hidden = NO;
        cell.model = model;
        return cell;
        
    }else if (type == 2) {
        IGNewsTableViewCell4 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell4"];
        cell.model = model;
        [cell.icon1Button addTarget:self action:@selector(twoIconButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.icon2Button addTarget:self action:@selector(twoIconButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.icon1Button.tag = 100 + indexPath.row;
        cell.icon2Button.tag = 1000 + indexPath.row;
        return cell;
        
    }else if (type == 3) {
        IGNewsTableViewCell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell1"];
        cell.videoImageView.hidden = YES;
        cell.model = model;
        return cell;
        
    }else if (type == 4) {
        IGNewsTableViewCell5 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell5"];
        cell.model = model;
        [cell.icon1Button addTarget:self action:@selector(fourIconButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.icon2Button addTarget:self action:@selector(fourIconButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.icon3Button addTarget:self action:@selector(fourIconButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.icon4Button addTarget:self action:@selector(fourIconButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.icon1Button.tag = 10 + indexPath.row;
        cell.icon2Button.tag = 100 + indexPath.row;
        cell.icon3Button.tag = 1000 + indexPath.row;
        cell.icon4Button.tag = 10000 + indexPath.row;
        return cell;
        
    }else if (type == 5){
        
        IGNewsTableViewCell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell2"];
        cell.model = model;
        return cell;
        
    }else if (type == 6) {
        IGNewsTableViewCell6 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell6"];
        cell.model = model;
        return cell;
        
    }else if (type == 9) {
        IGNewsTableViewCell1 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell1"];
        cell.videoImageView.hidden = NO;
        cell.model = model;
        return cell;
        
    }else if (type == 13) {
        IGNewsTableViewCell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell2"];
        cell.model = model;
        return cell;
        
    }else if (type == 14) {
        IGNewsTableViewCell3 *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsTableViewCell3"];
        cell.videoImageView.hidden = YES;
        cell.model = model;
        return cell;
    }
    
    return nil;
}



// cell点击
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IGNewsListModel *model = _dataArr[indexPath.row];
    NSString *model_type = model.model_type;
    NSInteger type = [model_type integerValue];
    
    if (type == 13) {
        self.pushBlock(model.infoalbum_id, model.infoalbum_title, type);
    }
    
    if (type == 3) {
        self.pushBlock(model.article_id, model.title, type);
    }
    
    if (type == 9) {
        self.pushBlock(model.vid, nil, type);
        [[IGHistoryList defaultIGHistoryList] saveDataAtLoacllyWithTitle:model.title img:model.img vid:model.vid];
    }
    
    if (type == 14) {
        self.pushBlock(model.article_id, model.title, type);
    }
    
    if (type == 1) {
        self.pushBlock(model.vid, nil, type);
    }
    
    if (type == 6) {
        
        NSArray *vidArr = [model.vid componentsSeparatedByString:@","];
        self.pushBlock(vidArr[0], nil, type);
    }
    
    if (type == 5) {
        
        self.pushBlock(model.tournament_id, nil, type);
        
    }


}

// cell4 button点击
- (void)twoIconButtonClick:(UIButton *)button
{
    if (button.tag < 1000) {
        
        IGNewsListModel *model = _dataArr[button.tag - 100];
        self.pushBlock(model.vid1, nil, 2);
        
    }else if (button.tag >= 1000){
        
        IGNewsListModel *model = _dataArr[button.tag - 1000];
        self.pushBlock(model.vid2, nil, 2);
    }
    
}

// cell5 button点击
- (void)fourIconButtonClick:(UIButton *)button
{
    if (button.tag < 100) {
        
        IGNewsListModel *model = _dataArr[button.tag - 10];
        self.pushBlock(model.vid1, nil, 2);
        
    }else if (button.tag < 1000){
        
        IGNewsListModel *model = _dataArr[button.tag - 100];
        self.pushBlock(model.vid2, nil, 2);
        
    }else if (button.tag < 10000){
        
        IGNewsListModel *model = _dataArr[button.tag - 1000];
        self.pushBlock(model.vid3, nil, 2);
        
    }else{
        
        IGNewsListModel *model = _dataArr[button.tag - 10000];
        self.pushBlock(model.vid4, nil, 2);
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
