//
//  IGNewsViewController.m
//  iGameApp
//
//  Created by lanou on 16/1/9.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGNewsViewController.h"
#import "IGCustomNavigationController.h"
#import "IGNewsCollectionViewCell.h"

#import "IGNewsAllViewController.h"
#import "IGNewsDOTAViewController.h"
#import "IGNewsStoreViewController.h"
#import "IGCSGOViewController.h"
#import "IGNewsLOLViewController.h"
#import "IGNewsHeroesViewController.h"

#import "IGNewsTournamentViewController.h"
#import "IGNewsArticleViewController.h"
#import "IGNewsVideoViewController.h"
#import "IG_GameGamesViewController.h"
#import "IGFindRadioPlayViewController.h"

@interface IGNewsViewController () <UIScrollViewDelegate>
@property (nonatomic, strong) IGCustomNavigationController *naviC;
@property (nonatomic ,strong) IGNewsAllViewController *allVC;
@property (nonatomic, strong) IGNewsDOTAViewController *dotaVC;
@property (nonatomic, strong) IGNewsStoreViewController *storeVC;
@property (nonatomic, strong) IGCSGOViewController *CSGOVC;
@property (nonatomic, strong) IGNewsLOLViewController *lolVC;
@property (nonatomic, strong) IGNewsHeroesViewController *heroesVC;
@property (nonatomic, strong) IGNewsParentViewController *parentVC;

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIScrollView *smallScrollView;
@property (nonatomic, assign) NSInteger scrollOldIndex;

@end

@implementation IGNewsViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _naviC.iconButton.hidden = NO;
    _naviC.backButton.hidden = YES;
    
    [self setNaviC];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [_smallScrollView removeFromSuperview];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];

    self.automaticallyAdjustsScrollViewInsets = NO;

    // 子视图
    [self setSubView];
    
    self.view.backgroundColor = [UIColor grayColor];
}

// 子视图
- (void)setSubView
{
    // 初始化scrollView
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight - 64 - 49)];
    _scrollView.contentSize = CGSizeMake(kScreenWidth * 6, kScreenHeight - 64 - 49);
    _scrollView.pagingEnabled = YES;
    _scrollView.delegate = self;
    _scrollView.bounces = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_scrollView];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(closeNotification) name:@"closeUserInteractionEnabled" object:nil];
    [center addObserver:self selector:@selector(openNotification) name:@"openUserInteractionEnabled" object:nil];

    
    // 初始化控制器
    _allVC = [[IGNewsAllViewController alloc] initWithNibName:@"IGNewsAllViewController" bundle:nil];
    _allVC.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight - 64 - 49);
    [_scrollView addSubview:_allVC.view];
    _scrollView.backgroundColor = _allVC.tableView.backgroundColor;
    
    [self pushFromViewController:[_allVC class]];

    
}

// 手势冲突
- (void)closeNotification
{
    _scrollView.userInteractionEnabled = NO;
}

- (void)openNotification
{
   _scrollView.userInteractionEnabled  = YES;
}



#pragma mark -- 导航栏
- (void)setNaviC
{
    _naviC = (IGCustomNavigationController *)self.navigationController;
    
    _smallScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(50, 10, kScreenWidth - 100, 25)];
    _smallScrollView.contentSize = CGSizeMake(500, 25);
    _smallScrollView.showsHorizontalScrollIndicator = NO;
    [_naviC.bgView addSubview:_smallScrollView];
    
    NSArray *titleArr = @[@"全部",@"DOTA",@"炉石传说",@"CS GO",@"英雄联盟",@"风暴英雄"];
    
    for (int i = 0; i < 6; i++) {
        
        if (i == 0) {
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(10, 0, 40, 25);
            [button setTitle:titleArr[0] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
            [_smallScrollView addSubview:button];
            button.tag = 100 + i;
            
        }else if (i == 1){
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(70, 0, 60, 25);
            [button setTitle:titleArr[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
            [_smallScrollView addSubview:button];
            button.tag = 100 + i;

        }else if (i == 2){
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(147, 0, 80, 25);
            [button setTitle:titleArr[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
            [_smallScrollView addSubview:button];
            button.tag = 100 + i;
            
        }else if (i == 3){
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(245, 0, 60, 25);
            [button setTitle:titleArr[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
            [_smallScrollView addSubview:button];
            button.tag = 100 + i;
            
        }else{
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(320 + 95 * (i - 4), 0, 80, 25);
            [button setTitle:titleArr[i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
            [_smallScrollView addSubview:button];
            button.tag = 100 + i;
        }

    }
    
    // 根据大的scrollview改变小的
    CGFloat x = _scrollView.contentOffset.x;
    NSInteger number = x / kScreenWidth;
    
    switch (number) {
        case 0:
        {
            [_smallScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
            UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
            [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:19];
            _scrollOldIndex = number;
        }
            break;
        case 1:
            
        {
            [_smallScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
            UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
            [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:19];
            _scrollOldIndex = number;

        }


            break;
        case 2:
            
        {
            [_smallScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
            UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
            [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:19];
            _scrollOldIndex = number;

        }


            break;
        case 3:
            
        {
            [_smallScrollView setContentOffset:CGPointMake(230, 0) animated:NO];
            UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
            [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:19];
            _scrollOldIndex = number;

        }
            
            break;
        case 4:
            
        {
            [_smallScrollView setContentOffset:CGPointMake(260, 0) animated:NO];
            UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
            [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:19];
            _scrollOldIndex = number;

        }
            
            break;
        case 5:
            
        {
            [_smallScrollView setContentOffset:CGPointMake(280, 0) animated:NO];
            UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
            [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:19];
            _scrollOldIndex = number;

        }
            
            break;

            
        default:
            break;
    }
    
    
}

// 导航栏scrollView点击方法
- (void)buttonClick:(UIButton *)button
{
    switch (button.tag) {
        case 100:
            
            [_scrollView setContentOffset:CGPointMake(kScreenWidth * (button.tag - 100), 0) animated:YES];
            
            break;
        case 101:
            
            [_scrollView setContentOffset:CGPointMake(kScreenWidth * (button.tag - 100), 0) animated:YES];

            break;
        case 102:
            
            [_scrollView setContentOffset:CGPointMake(kScreenWidth * (button.tag - 100), 0) animated:YES];

            break;
        case 103:
            
            [_scrollView setContentOffset:CGPointMake(kScreenWidth * (button.tag - 100), 0) animated:YES];

            break;
        case 104:
            
            [_scrollView setContentOffset:CGPointMake(kScreenWidth * (button.tag - 100), 0) animated:YES];

            break;
        case 105:
            
            [_scrollView setContentOffset:CGPointMake(kScreenWidth * (button.tag - 100), 0) animated:YES];

            break;
            
        default:
            break;
    }
    
    
    
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    CGFloat x = scrollView.contentOffset.x;
    if (x < 0) {
        x = 0;
    }else if (x > kScreenWidth * 5){
        
        x = kScreenWidth * 5;
    }
    
    NSInteger number = x / kScreenWidth;
    
    switch (number) {
        case 0:
        {
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [_smallScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
                
                UIButton *oldButton = (UIButton *)[_smallScrollView viewWithTag:100 + _scrollOldIndex];
                [oldButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                oldButton.titleLabel.font = [UIFont systemFontOfSize:18];
                
                UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
                [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfSize:19];
                
                _scrollOldIndex = number;
                
            }];
        }
            break;
        case 1:
        {
            if (!_dotaVC) {
                _dotaVC = [[IGNewsDOTAViewController alloc] initWithNibName:@"IGNewsDOTAViewController" bundle:nil];
                _dotaVC.view.frame = CGRectMake(kScreenWidth, 0, kScreenWidth, kScreenHeight - 64 - 49);
                
                [_scrollView addSubview:_dotaVC.view];
                
                [self pushFromViewController:[_dotaVC class]];
            }
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [_smallScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
                UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
                [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfSize:19];
                
                UIButton *oldButton = (UIButton *)[_smallScrollView viewWithTag:100 + _scrollOldIndex];
                [oldButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                oldButton.titleLabel.font = [UIFont systemFontOfSize:18];
                
                _scrollOldIndex = number;
                
            }];
            
        }
            break;
        case 2:
        {
            if (!_storeVC) {
                _storeVC = [[IGNewsStoreViewController alloc] initWithNibName:@"IGNewsStoreViewController" bundle:nil];
                _storeVC.view.frame = CGRectMake(kScreenWidth * 2, 0, kScreenWidth, kScreenHeight - 64 - 49);
                [_scrollView addSubview:_storeVC.view];
                
                [self pushFromViewController:[_storeVC class]];
                
                
            }
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [_smallScrollView setContentOffset:CGPointMake(20, 0) animated:YES];
                UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
                [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfSize:19];
                
                UIButton *oldButton = (UIButton *)[_smallScrollView viewWithTag:100 + _scrollOldIndex];
                [oldButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                oldButton.titleLabel.font = [UIFont systemFontOfSize:18];
                
                _scrollOldIndex = number;
                
            }];
        }
            break;
        case 3:
        {
            if (!_CSGOVC) {
                _CSGOVC = [[IGCSGOViewController alloc] initWithNibName:@"IGCSGOViewController" bundle:nil];
                _CSGOVC.view.frame = CGRectMake(kScreenWidth * 3, 0, kScreenWidth, kScreenHeight - 64 - 49);
                [_scrollView addSubview:_CSGOVC.view];
                
                [self pushFromViewController:[_CSGOVC class]];
                
            }
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [_smallScrollView setContentOffset:CGPointMake(230, 0) animated:YES];
                UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
                [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfSize:19];
                
                UIButton *oldButton = (UIButton *)[_smallScrollView viewWithTag:100 + _scrollOldIndex];
                [oldButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                oldButton.titleLabel.font = [UIFont systemFontOfSize:18];
                
                _scrollOldIndex = number;
                
            }];
            
            
            
        }
            break;
        case 4:
        {
            if (!_lolVC) {
                _lolVC = [[IGNewsLOLViewController alloc] initWithNibName:@"IGNewsLOLViewController" bundle:nil];
                _lolVC.view.frame = CGRectMake(kScreenWidth * 4, 0, kScreenWidth, kScreenHeight - 64 - 49);
                [_scrollView addSubview:_lolVC.view];
                
                [self pushFromViewController:[_lolVC class]];
                
            }
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [_smallScrollView setContentOffset:CGPointMake(260, 0) animated:YES];
                UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
                [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfSize:19];
                
                UIButton *oldButton = (UIButton *)[_smallScrollView viewWithTag:100 + _scrollOldIndex];
                [oldButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                oldButton.titleLabel.font = [UIFont systemFontOfSize:18];
                
                _scrollOldIndex = number;
                
            }];
            
            
        }
            break;
        case 5:
        {
            if (!_heroesVC) {
                _heroesVC = [[IGNewsHeroesViewController alloc] initWithNibName:@"IGNewsHeroesViewController" bundle:nil];
                _heroesVC.view.frame = CGRectMake(kScreenWidth * 5, 0, kScreenWidth, kScreenHeight - 64 - 49);
                [_scrollView addSubview:_heroesVC.view];
                
                [self pushFromViewController:[_heroesVC class]];
                
            }
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [_smallScrollView setContentOffset:CGPointMake(280, 0) animated:YES];
                
                UIButton *oldButton = (UIButton *)[_smallScrollView viewWithTag:100 + _scrollOldIndex];
                [oldButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                oldButton.titleLabel.font = [UIFont systemFontOfSize:18];
                
                UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
                [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfSize:19];
                
                _scrollOldIndex = number;
                
            }];
            
            
        }
            break;
        default:
            break;
    }

    
    
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat x = scrollView.contentOffset.x;
    if (x < 0) {
        x = 0;
    }else if (x > kScreenWidth * 5){
        
        x = kScreenWidth * 5;
    }

    NSInteger number = x / kScreenWidth;
    
    switch (number) {
        case 0:
        {
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [_smallScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
            
                UIButton *oldButton = (UIButton *)[_smallScrollView viewWithTag:100 + _scrollOldIndex];
                [oldButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                oldButton.titleLabel.font = [UIFont systemFontOfSize:18];
                
                UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
                [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfSize:19];
                
                _scrollOldIndex = number;

            }];
        }
            break;
        case 1:
        {
            if (!_dotaVC) {
                _dotaVC = [[IGNewsDOTAViewController alloc] initWithNibName:@"IGNewsDOTAViewController" bundle:nil];
                _dotaVC.view.frame = CGRectMake(kScreenWidth, 0, kScreenWidth, kScreenHeight - 64 - 49);

                [_scrollView addSubview:_dotaVC.view];
                
                [self pushFromViewController:[_dotaVC class]];
            }
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [_smallScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
                UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
                [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfSize:19];
                
                UIButton *oldButton = (UIButton *)[_smallScrollView viewWithTag:100 + _scrollOldIndex];
                [oldButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                oldButton.titleLabel.font = [UIFont systemFontOfSize:18];
                
                _scrollOldIndex = number;
                
            }];
            
        }
            break;
        case 2:
        {
            if (!_storeVC) {
                _storeVC = [[IGNewsStoreViewController alloc] initWithNibName:@"IGNewsStoreViewController" bundle:nil];
                _storeVC.view.frame = CGRectMake(kScreenWidth * 2, 0, kScreenWidth, kScreenHeight - 64 - 49);
                [_scrollView addSubview:_storeVC.view];
                
                [self pushFromViewController:[_storeVC class]];

            }
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [_smallScrollView setContentOffset:CGPointMake(20, 0) animated:YES];
                UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
                [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfSize:19];
                
                UIButton *oldButton = (UIButton *)[_smallScrollView viewWithTag:100 + _scrollOldIndex];
                [oldButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                oldButton.titleLabel.font = [UIFont systemFontOfSize:18];
                
                _scrollOldIndex = number;
                
            }];
        }
            break;
        case 3:
        {
            if (!_CSGOVC) {
                _CSGOVC = [[IGCSGOViewController alloc] initWithNibName:@"IGCSGOViewController" bundle:nil];
                _CSGOVC.view.frame = CGRectMake(kScreenWidth * 3, 0, kScreenWidth, kScreenHeight - 64 - 49);
                [_scrollView addSubview:_CSGOVC.view];
                
                [self pushFromViewController:[_CSGOVC class]];

            }
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [_smallScrollView setContentOffset:CGPointMake(230, 0) animated:YES];
                UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
                [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfSize:19];
                
                UIButton *oldButton = (UIButton *)[_smallScrollView viewWithTag:100 + _scrollOldIndex];
                [oldButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                oldButton.titleLabel.font = [UIFont systemFontOfSize:18];
                
                _scrollOldIndex = number;

            }];
            
            

        }
            break;
        case 4:
        {
            if (!_lolVC) {
                _lolVC = [[IGNewsLOLViewController alloc] initWithNibName:@"IGNewsLOLViewController" bundle:nil];
                _lolVC.view.frame = CGRectMake(kScreenWidth * 4, 0, kScreenWidth, kScreenHeight - 64 - 49);
                [_scrollView addSubview:_lolVC.view];
                
                [self pushFromViewController:[_lolVC class]];

            }
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [_smallScrollView setContentOffset:CGPointMake(260, 0) animated:YES];
                UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
                [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfSize:19];
                
                UIButton *oldButton = (UIButton *)[_smallScrollView viewWithTag:100 + _scrollOldIndex];
                [oldButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                oldButton.titleLabel.font = [UIFont systemFontOfSize:18];
                
                _scrollOldIndex = number;
                
            }];


        }
            break;
        case 5:
        {
            if (!_heroesVC) {
                _heroesVC = [[IGNewsHeroesViewController alloc] initWithNibName:@"IGNewsHeroesViewController" bundle:nil];
                _heroesVC.view.frame = CGRectMake(kScreenWidth * 5, 0, kScreenWidth, kScreenHeight - 64 - 49);
                [_scrollView addSubview:_heroesVC.view];
                
                [self pushFromViewController:[_heroesVC class]];

            }
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [_smallScrollView setContentOffset:CGPointMake(280, 0) animated:YES];
                
                UIButton *oldButton = (UIButton *)[_smallScrollView viewWithTag:100 + _scrollOldIndex];
                [oldButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                oldButton.titleLabel.font = [UIFont systemFontOfSize:18];
                
                UIButton *button = (UIButton *)[_smallScrollView viewWithTag:100 + number];
                [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfSize:19];
                
                _scrollOldIndex = number;
                
            }];


        }
            break;
        default:
            break;
    }
}

- (void)pushFromViewController:(Class)class
{
    __weak IGNewsViewController *newsVC = self;
    
    if ([NSStringFromClass(class) isEqualToString:@"IGNewsAllViewController"]) {
        _parentVC = _allVC;
    }

    if ([NSStringFromClass(class) isEqualToString:@"IGNewsDOTAViewController"]) {
        _parentVC = _dotaVC;
    }
    
    if ([NSStringFromClass(class) isEqualToString:@"IGNewsStoreViewController"]) {
        _parentVC = _storeVC;
    }

    if ([NSStringFromClass(class) isEqualToString:@"IGCSGOViewController"]) {
        _parentVC = _CSGOVC;
    }

    if ([NSStringFromClass(class) isEqualToString:@"IGNewsLOLViewController"]) {
        _parentVC = _lolVC;
    }
    
    if ([NSStringFromClass(class) isEqualToString:@"IGNewsHeroesViewController"]) {
        _parentVC = _heroesVC;
    }
    
    _parentVC.pushBlock = ^void (NSString *infoalbum_id, NSString *infoalbum_title, NSInteger model_type){
        
        if (model_type == 13) {
            IGNewsTournamentViewController *tournamentVC = [[IGNewsTournamentViewController alloc] initWithNibName:@"IGNewsTournamentViewController" bundle:nil];
            tournamentVC.infoalbum_id = infoalbum_id;
            tournamentVC.infoalbum_title = infoalbum_title;
            [newsVC.navigationController pushViewController:tournamentVC animated:YES];
        }
        
        if (model_type == 3) {
            IGNewsArticleViewController *articleVC = [[IGNewsArticleViewController alloc] initWithNibName:@"IGNewsArticleViewController" bundle:nil];
            articleVC.article_id = infoalbum_id;
            [newsVC.navigationController pushViewController:articleVC animated:YES];
            
        }
        
        if (model_type == 9) {
            
            IGFindRadioPlayViewController *videoVC = [[IGFindRadioPlayViewController alloc] initWithNibName:@"IGFindRadioPlayViewController" bundle:nil];
            videoVC.vid = infoalbum_id;
            videoVC.isFindPushHere = YES;
            [newsVC.navigationController pushViewController:videoVC animated:YES];
            
            
        }
        
        if (model_type == 14) {
            
            IGNewsArticleViewController *articleVC = [[IGNewsArticleViewController alloc] initWithNibName:@"IGNewsArticleViewController" bundle:nil];
            articleVC.article_id = infoalbum_id;
            [newsVC.navigationController pushViewController:articleVC animated:YES];
            
        }
        
        if (model_type == 1) {
            
            IGFindRadioPlayViewController *videoVC = [[IGFindRadioPlayViewController alloc] initWithNibName:@"IGFindRadioPlayViewController" bundle:nil];
            videoVC.vid = infoalbum_id;
            videoVC.isFindPushHere = YES;
            [newsVC.navigationController pushViewController:videoVC animated:YES];
            
        }
        
        if (model_type == 2) {
            
            IGFindRadioPlayViewController *videoVC = [[IGFindRadioPlayViewController alloc] initWithNibName:@"IGFindRadioPlayViewController" bundle:nil];
            videoVC.vid = infoalbum_id;
            videoVC.isFindPushHere = YES;
            [newsVC.navigationController pushViewController:videoVC animated:YES];
        }
        
        if (model_type == 6) {
            
            IGFindRadioPlayViewController *videoVC = [[IGFindRadioPlayViewController alloc] initWithNibName:@"IGFindRadioPlayViewController" bundle:nil];
            videoVC.vid = infoalbum_id;
            videoVC.isFindPushHere = YES;
            [newsVC.navigationController pushViewController:videoVC animated:YES];

        }
        
        if (model_type == 5) {
            
            IG_GameGamesViewController *gameVC = [[IG_GameGamesViewController alloc] init];
            gameVC.tournament_id = infoalbum_id;
            gameVC.title = infoalbum_title;
            [newsVC.navigationController pushViewController:gameVC animated:YES];
           
        }

        
        
    };
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
