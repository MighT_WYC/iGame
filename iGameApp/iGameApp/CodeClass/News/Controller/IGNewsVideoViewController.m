//
//  IGNewsVideoViewController.m
//  iGameApp
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGNewsVideoViewController.h"
#import "IGCustomNavigationController.h"
#import "IGNewsVideoTableViewCell.h"
#import "IGNewsVideoHeaderView.h"
#import "IG_iGameRoundCommentModel.h"
#import "IG_RoundCommentTableViewCell.h"
#import "IGNewsCommentView.h"
#import "IGLoginViewController.h"

@interface IGNewsVideoViewController () <UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate, UMSocialUIDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IMYWebView *webView;

@property (weak, nonatomic) IBOutlet UILabel *bgLabel;
@property (weak, nonatomic) IBOutlet UITextField *commentTF;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (nonatomic, strong) IGNewsCommentView *commentView;
@property (weak, nonatomic) IBOutlet UILabel *sofaLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sofaImageView;


// 评论数
@property (nonatomic, assign) NSInteger commentNumber;
@property (nonatomic, strong) NSMutableArray * commentArr;


@property (nonatomic, strong) IGCustomNavigationController *naviC;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) IGNewsVideoHeaderView *headerView;
@property (nonatomic, strong) UIButton *shareButton;



@end

@implementation IGNewsVideoViewController

- (void)viewWillAppear:(BOOL)animated
{
    _naviC.backButton.hidden = NO;
    _naviC.iconButton.hidden = YES;
    _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _shareButton.frame = CGRectMake(kScreenWidth - 40, 9.5, 25, 25);
    [_shareButton setBackgroundImage:[UIImage imageNamed:@"shareInfo"] forState:UIControlStateNormal];
    [_shareButton addTarget:self action:@selector(shareButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [_naviC.bgView addSubview:_shareButton];

    // 隐藏tabBar
    self.tabBarController.tabBar.hidden = YES;

    // 评论
    _commentView = [[UINib nibWithNibName:@"iGNewsCommentView" bundle:nil] instantiateWithOwner:self options:nil].firstObject;
    _commentView.height = 165;
    [_commentView.cancelButton addTarget:self action:@selector(cancelButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [_commentView.sendButton addTarget:self action:@selector(sendButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _commentTF.inputAccessoryView = _commentView;
    _commentTF.delegate = self;

}

- (void)viewWillDisappear:(BOOL)animated
{
    _naviC.backButton.hidden = YES;
    _naviC.iconButton.hidden = NO;
    [_shareButton removeFromSuperview];
    
    // 显示tabBar
    self.tabBarController.tabBar.hidden = NO;

}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = _tableView.backgroundColor;
    
    _naviC = (IGCustomNavigationController *)self.navigationController;
    
    _tableView.autoresizesSubviews = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    // 注册
    [_tableView registerNib:[UINib nibWithNibName:@"IGNewsVideoTableViewCell" bundle:nil] forCellReuseIdentifier:@"IGNewsVideoTableViewCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"IG_RoundCommentTableViewCell" bundle:nil] forCellReuseIdentifier:@"IG_RoundCommentTableViewCell"];

    
    // 注册表头
    _headerView = [[UINib nibWithNibName:@"IGNewsVideoHeaderView" bundle:nil] instantiateWithOwner:self options:nil].firstObject;
   
    
    // 播放视频
    [self videoRequest];
    
    // 初始化数组
    _dataArr = [NSMutableArray array];
    _commentArr = [NSMutableArray array];

    
    

}
#pragma mark -- 视频请求
- (void)videoRequest
{
    NSMutableDictionary *parDic = [NSMutableDictionary dictionary];
    parDic[@"vid"] = _vid;
    
    
    [IGAFNetworking requestWithUrlString:@"http://www.imbatv.cn/api_2_4_0/VideoDetail?" parDic:parDic metho:GET finish:^(NSDictionary *dataDic) {
        
        NSDictionary *dic = dataDic[@"data"];
        NSString *playUrl = dic[@"player_url"];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:playUrl]];
        [_webView loadRequest:request];
        
        _headerView.titleLabel.text = dic[@"title"];
        [_headerView.titleLabel sizeToFit];
        _headerView.descLabel.text = dic[@"desc"];
        [_headerView.descLabel sizeToFit];
        
        CGRect newFrame = _headerView.frame;
        newFrame.size.height = _headerView.titleLabel.height + _headerView.descLabel.height + 90;
        _headerView.frame = newFrame;
        _tableView.tableHeaderView = _headerView;
        
        
    } error:^(NSError *requestError) {
        NSLog(@"%@",requestError);
    }];
}

#pragma mark -- 数据请求
- (void)requestData
{
    NSMutableDictionary *parDic = [NSMutableDictionary dictionary];
    parDic[@"match_id"] = @"-1";
    parDic[@"video_id"] = _vid;
    
    [IGAFNetworking requestWithUrlString:@"http://www.imbatv.cn/api_2_4_0/get_related_videos_list?" parDic:parDic metho:GET finish:^(NSDictionary *dataDic) {
        
        NSArray *dataArr = dataDic[@"data"];
        for (NSDictionary *oneDic in dataArr) {
            IGNewsVideoModel *model = [[IGNewsVideoModel alloc] init];
            [model setValuesForKeysWithDictionary:oneDic];
            [_dataArr addObject:model];
        }
        
        
        [_tableView reloadData];
        
    } error:^(NSError *requestError) {
        NSLog(@"%@",requestError);
    }];
    
}

- (void)requestComment
{
    NSMutableDictionary *parDic = [NSMutableDictionary dictionary];
    parDic[@"video_id"] = _vid;
    parDic[@"num"] = @"10";
    parDic[@"start"] = @"0";
    
    [IGAFNetworking requestWithUrlString:@"http://www.imbatv.cn/api_2_4_0/getVideoComment?" parDic:parDic metho:GET finish:^(NSDictionary *dataDic) {
        
        NSDictionary *dic = dataDic[@"data"];
        NSString *commentStr = dic[@"counts"];
        _commentNumber = [commentStr integerValue];
        
        if (_commentNumber == 0) {
            
            _sofaLabel.text = @"沙发";
            _sofaImageView.image = [UIImage imageNamed:@"iconfont-iconfontshafa"];
        }else{
            
            _sofaLabel.text = [NSString stringWithFormat:@"%ld",_commentNumber];
            _sofaImageView.image = [UIImage imageNamed:@"comment"];
            
        }

        
        for (NSDictionary *oneDic in dic[@"comment"]) {
            
            IG_iGameRoundCommentModel *model = [[IG_iGameRoundCommentModel alloc] init];
            [model setValuesForKeysWithDictionary:oneDic];
            [_commentArr addObject:model];
            
        }
        
        [_tableView reloadData];
        
    } error:^(NSError *requestError) {
        NSLog(@"%@",requestError);
    }];
    
    
}


#pragma mark -- webView
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self requestData];
    [self requestComment];
    
}

#pragma mark -- tableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_commentArr.count > 0) {
        
        return 2;
    }else{
        return 1;
    }

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section > 0) {
        
        return _commentArr.count;
        
    }else{
        
        return _dataArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section > 0) {
        
        IG_iGameRoundCommentModel *model = _commentArr[indexPath.row];
        IG_RoundCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"IG_RoundCommentTableViewCell"];
        cell.model = model;
        return cell;
        
    }else{

        if (_dataArr.count != 0) {
            
            IGNewsVideoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"IGNewsVideoTableViewCell"];
            IGNewsVideoModel *model = _dataArr[indexPath.row];
            cell.model = model;
            return cell;
        }else{
            return nil;
            
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section > 0) {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 25)];
        label.text = [NSString stringWithFormat:@"评论 (%ld)",_commentNumber];
        label.backgroundColor = [UIColor grayColor];
        label.textAlignment = NSTextAlignmentCenter;
        return label;
        
    }else{

        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 25)];
        label.text = @"相关视频";
        label.backgroundColor = [UIColor grayColor];
        label.textAlignment = NSTextAlignmentCenter;
        return label;
    
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section > 0) {
        
        return 90;
        
    }else{
        return 80;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        
        IGNewsVideoModel *model = _dataArr[indexPath.row];
        
        IGNewsVideoViewController *videoVC = [[IGNewsVideoViewController alloc] initWithNibName:@"IGNewsVideoViewController" bundle:nil];
        videoVC.vid = model.vid;
        [self.navigationController pushViewController:videoVC animated:YES];
    }
    
    
}

// 导航栏分享按钮
- (void)shareButtonClick:(UIButton *)button
{
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:@"569748dd67e58e9965001f37"
                                      shareText:@"欢迎使用iGame"
                                     shareImage:[UIImage imageNamed:@"icon.png"]
                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToWechatSession,UMShareToQQ,UMShareToEmail,UMShareToWechatFavorite,UMShareToQQ,UMShareToQzone,UMShareToSms, nil]
                                       delegate:self];
    
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    IG_UserMessage *user = [IG_UserMessage shareUserMessage];
    
    BOOL islogin = user.isLoading;
    
    if (islogin == YES) {
        [self performSelector:@selector(setFristResponder) withObject:nil afterDelay:1];
    }else{
        
        [textField resignFirstResponder];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"请先登陆" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        
        [alertView show];
        
        IGLoginViewController *loginVC = [[IGLoginViewController alloc] initWithNibName:@"IGLoginViewController" bundle:nil];
        [self presentViewController:loginVC animated:YES completion:nil];
        
        
    }
}

- (void)setFristResponder
{
    [_commentView.textView becomeFirstResponder];
    [_commentTF resignFirstResponder];
}


// 取消评论
- (void)cancelButtonClick:(UIButton *)button
{
    
    [_commentView.textView resignFirstResponder];
    [_commentTF resignFirstResponder];
    _commentTF.text = _commentView.textView.text;
    
}

// 发送评论
- (void)sendButtonClick:(UIButton *)button
{
    [self sendComment];
    
    
}

// 发送评论
- (void)sendComment
{
    [_commentView.textView resignFirstResponder];
    [_commentTF resignFirstResponder];
    
    NSMutableDictionary *parDic = [NSMutableDictionary dictionary];
    parDic[@"article_id"] = _vid;
    parDic[@"city"] = [@"上海市" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    parDic[@"content"] = [_commentView.textView.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    parDic[@"uid"] = @"348319";
    
    [IGAFNetworking newRequestWithUrlString:@"http://www.imbatv.cn/api_2_4_0/addNewsComment" parDic:parDic method:POST finish:^(NSDictionary *dataDic) {
        [self requestComment];
        _commentView.textView.text = nil;
    } error:^(NSError *requestError) {
        NSLog(@"%@",requestError);
    }];
    
}

// 滚动到评论
- (IBAction)scrollToComment:(UIButton *)sender {
    
    if (_commentNumber != 0) {
        
        if (_tableView.contentOffset.y == 0) {
            
            [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }else{
            
            [_tableView setContentOffset:CGPointMake(0, 0) animated:YES];
            
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
