//
//  IGNewsVideoViewController.h
//  iGameApp
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGNewsVideoViewController : UIViewController

@property (nonatomic, strong) NSString *vid;

@end
