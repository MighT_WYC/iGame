//
//  IGNewsArticleViewController.h
//  iGameApp
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGNewsArticleViewController : UIViewController

@property (nonatomic, strong) NSString *article_id;

@end
