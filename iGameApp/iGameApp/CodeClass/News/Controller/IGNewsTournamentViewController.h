//
//  IGNewsTournamentViewController.h
//  iGameApp
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGNewsTournamentViewController : UIViewController

@property (nonatomic, strong) NSString *infoalbum_id;
@property (nonatomic, strong) NSString *infoalbum_title;

@end
