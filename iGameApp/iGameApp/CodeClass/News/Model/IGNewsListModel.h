//
//  IGNewsListModel.h
//  iGameApp
//
//  Created by lanou on 16/1/13.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGNewsListModel : NSObject

@property (nullable, nonatomic, retain) NSString *article_id;
@property (nullable, nonatomic, retain) NSString *img;
@property (nullable, nonatomic, retain) NSString *info_id;
@property (nullable, nonatomic, retain) NSString *infoalbum_id;
@property (nullable, nonatomic, retain) NSString *infoalbum_image;
@property (nullable, nonatomic, retain) NSString *infoalbum_title;
@property (nullable, nonatomic, retain) NSString *model_type;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *vid;
@property (nullable, nonatomic, retain) NSString *tournament_id;


@property (nullable, nonatomic, retain) NSString *img1;
@property (nullable, nonatomic, retain) NSString *img2;
@property (nullable, nonatomic, retain) NSString *img3;
@property (nullable, nonatomic, retain) NSString *img4;
@property (nullable, nonatomic, retain) NSString *title1;
@property (nullable, nonatomic, retain) NSString *title2;
@property (nullable, nonatomic, retain) NSString *title3;
@property (nullable, nonatomic, retain) NSString *title4;
@property (nullable, nonatomic, retain) NSString *vid1;
@property (nullable, nonatomic, retain) NSString *vid2;
@property (nullable, nonatomic, retain) NSString *vid3;
@property (nullable, nonatomic, retain) NSString *vid4;

@property (nullable, nonatomic, retain) NSString *team_logo_A;
@property (nullable, nonatomic, retain) NSString *team_logo_B;
@property (nullable, nonatomic, retain) NSString *team_name_A;
@property (nullable, nonatomic, retain) NSString *team_name_B;
@property (nullable, nonatomic, retain) NSString *tournament_name;
@property (nullable, nonatomic, retain) NSString *score;
@property (nullable, nonatomic, retain) NSString *match_state;



@end
