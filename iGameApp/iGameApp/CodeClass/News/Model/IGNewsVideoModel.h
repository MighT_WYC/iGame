//
//  IGNewsVideoModel.h
//  iGameApp
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGNewsVideoModel : NSObject

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *vid;

@end
