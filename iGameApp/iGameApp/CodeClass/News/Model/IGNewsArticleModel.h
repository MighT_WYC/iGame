//
//  IGNewsArticleModel.h
//  iGameApp
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGNewsArticleModel : NSObject

@property (nonatomic, strong) NSString *addtime;

@property (nonatomic, strong) NSString *article_id;

@property (nonatomic, strong) NSString *article_title;


@end
