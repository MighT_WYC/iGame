//
//  IGNewsCommentModel.h
//  iGameApp
//
//  Created by lanou on 16/1/18.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGNewsCommentModel : NSObject

@property (nonatomic, strong) NSString *user_img;

@property (nonatomic, strong) NSString *nickname;

@property (nonatomic, strong) NSString *city;

@property (nonatomic, strong) NSString *time;

@property (nonatomic, strong) NSString *content;

@property (nonatomic, strong) NSString *top;

@property (nonatomic, assign) BOOL is_top;


@end
