//
//  IGNewsListModel.m
//  iGameApp
//
//  Created by lanou on 16/1/13.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGNewsListModel.h"

@implementation IGNewsListModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
    if ([key isEqualToString:@"videos"]) {
        
        NSArray *videosArr = [NSArray arrayWithArray:value];
        
        if (videosArr.count == 2) {
            
            _img1 = videosArr[0][@"img"];
            _title1 = videosArr[0][@"title"];
            _vid1 = videosArr[0][@"vid"];
            
            _img2 = videosArr[1][@"img"];
            _title2 = videosArr[1][@"title"];
            _vid2 = videosArr[1][@"vid"];
            
        }else if (videosArr.count == 4) {
            
            _img1 = videosArr[0][@"img"];
            _title1 = videosArr[0][@"title"];
            _vid1 = videosArr[0][@"vid"];
            
            _img2 = videosArr[1][@"img"];
            _title2 = videosArr[1][@"title"];
            _vid2 = videosArr[1][@"vid"];
            
            _img3 = videosArr[2][@"img"];
            _title3 = videosArr[2][@"title"];
            _vid3 = videosArr[2][@"vid"];
            
            _img4 = videosArr[3][@"img"];
            _title4 = videosArr[3][@"title"];
            _vid4 = videosArr[3][@"vid"];
            
        }
    }
    
    if ([key isEqualToString:@"match"]) {
        
        _team_logo_A = value[@"team_logo_A"];
        _team_logo_B = value[@"team_logo_B"];
        _team_name_A = value[@"team_name_A"];
        _team_name_B = value[@"team_name_B"];
        _tournament_name = value[@"tournament_name"];
        _score = value[@"score"];
        _match_state = value[@"match_state"];
        _vid = value[@"vid"];
    }
}

@end
