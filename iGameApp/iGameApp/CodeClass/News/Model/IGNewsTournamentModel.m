//
//  IGNewsTournamentModel.m
//  iGameApp
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGNewsTournamentModel.h"

@implementation IGNewsTournamentModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"match"]) {
        
        _team_logo_A = value[@"team_logo_A"];
        _team_logo_B = value[@"team_logo_B"];
        _team_name_A = value[@"team_name_A"];
        _team_name_B = value[@"team_name_B"];
        _tournament_name = value[@"tournament_name"];
        _score = value[@"score"];
        _match_state = value[@"match_state"];
        _vid = value[@"vid"];
    }
    
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    
    if ([key isEqualToString:@"id"]) {
        
        _tournament_id = value;
        
    }
    
    
}

@end
