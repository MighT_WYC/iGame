//
//  IGNewsTournamentModel.h
//  iGameApp
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGNewsTournamentModel : NSObject

@property (nullable, nonatomic, strong) NSString *image;
@property (nullable, nonatomic, retain) NSString *tournament_id;
@property (nullable, nonatomic, retain) NSString *name;


@property (nullable, nonatomic, strong) NSString *img;

@property (nullable, nonatomic, strong) NSString *title;


@property (nullable, nonatomic, strong) NSString *model_type;

@property (nullable, nonatomic, strong) NSString *vid;

@property (nullable, nonatomic, strong) NSString *article_id;

@property (nullable, nonatomic, retain) NSString *team_logo_A;
@property (nullable, nonatomic, retain) NSString *team_logo_B;
@property (nullable, nonatomic, retain) NSString *team_name_A;
@property (nullable, nonatomic, retain) NSString *team_name_B;
@property (nullable, nonatomic, retain) NSString *tournament_name;
@property (nullable, nonatomic, retain) NSString *score;
@property (nullable, nonatomic, retain) NSString *match_state;


@end
