//
//  IGNewsCommentView.h
//  iGameApp
//
//  Created by lanou on 16/1/18.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGNewsCommentView : UIView

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@property (weak, nonatomic) IBOutlet UITextView *textView;



@end
