//
//  IGNewsTableViewCell1.h
//  iGameApp
//
//  Created by lanou on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGNewsListModel.h"
#import "IGNewsTournamentModel.h"
@interface IGNewsTableViewCell1 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (nonatomic, strong) IGNewsListModel *model;
@property (nonatomic, strong) HistoryList *list;
@property (nonatomic, strong) IGNewsTournamentModel *tournamentModel;

@property (weak, nonatomic) IBOutlet UIImageView *videoImageView;

@end
