//
//  IGNewsTableViewCell6.h
//  iGameApp
//
//  Created by lanou on 16/1/13.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGNewsListModel.h"
#import "IGNewsTournamentModel.h"
@interface IGNewsTableViewCell6 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *team1ImageView;

@property (weak, nonatomic) IBOutlet UIImageView *team2ImageView;

@property (weak, nonatomic) IBOutlet UILabel *team1Label;

@property (weak, nonatomic) IBOutlet UILabel *team2Label;

@property (weak, nonatomic) IBOutlet UILabel *matchLabel;

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

@property (weak, nonatomic) IBOutlet UILabel *stateLabel;


@property (nonatomic, strong) IGNewsListModel *model;
@property (nonatomic, strong) IGNewsTournamentModel *tournamentModel;

@end
