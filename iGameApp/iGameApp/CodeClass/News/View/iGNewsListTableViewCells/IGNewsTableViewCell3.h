//
//  IGNewsTableViewCell3.h
//  iGameApp
//
//  Created by lanou on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGNewsListModel.h"
#import "IGNewsTournamentModel.h"

@interface IGNewsTableViewCell3 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIImageView *videoImageView;

@property (nonatomic, strong) IGNewsListModel *model;
@property (nonatomic, strong) IGNewsTournamentModel *tournamentModel;

@end
