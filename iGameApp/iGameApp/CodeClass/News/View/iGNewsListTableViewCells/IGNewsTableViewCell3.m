//
//  IGNewsTableViewCell3.m
//  iGameApp
//
//  Created by lanou on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGNewsTableViewCell3.h"

@implementation IGNewsTableViewCell3

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(IGNewsListModel *)model
{
    _model = model;
    
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:model.img]];
    _titleLabel.text = model.title;
}

- (void)setTournamentModel:(IGNewsTournamentModel *)tournamentModel
{
    _tournamentModel = tournamentModel;
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:tournamentModel.img]];
    _titleLabel.text = tournamentModel.title;
    
    
}

- (void)setFrame:(CGRect)frame
{
    CGRect newFrame = frame;
    
    newFrame.origin.x += 5;
    newFrame.size.width -= 10;
    newFrame.origin.y += 2.5;
    newFrame.size.height -= 5;
    
    [super setFrame:newFrame];
}

@end
