//
//  IGNewsTableViewCell6.m
//  iGameApp
//
//  Created by lanou on 16/1/13.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGNewsTableViewCell6.h"

@implementation IGNewsTableViewCell6

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(IGNewsListModel *)model
{
    _model = model;
    
    [_team1ImageView sd_setImageWithURL:[NSURL URLWithString:model.team_logo_A]];
    [_team2ImageView sd_setImageWithURL:[NSURL URLWithString:model.team_logo_B]];
    
    _team1Label.text = model.team_name_A;
    _team2Label.text = model.team_name_B;

    _matchLabel.text = model.tournament_name;
    _scoreLabel.text = model.score;
    
    if ([model.match_state isEqualToString:@"4"]) {
        _stateLabel.text = @"回看";
        _stateLabel.textColor = [UIColor blueColor];
    }

}

- (void)setTournamentModel:(IGNewsTournamentModel *)tournamentModel
{
    _tournamentModel = tournamentModel;
    
    [_team1ImageView sd_setImageWithURL:[NSURL URLWithString:tournamentModel.team_logo_A]];
    [_team2ImageView sd_setImageWithURL:[NSURL URLWithString:tournamentModel.team_logo_B]];
    
    _team1Label.text = tournamentModel.team_name_A;
    _team2Label.text = tournamentModel.team_name_B;
    
    _matchLabel.text = tournamentModel.tournament_name;
    _scoreLabel.text = tournamentModel.score;
    
    NSString *state = [NSString stringWithFormat:@"%@",tournamentModel.match_state];
    if ([state isEqualToString:@"2"]) {
        _stateLabel.text = @"正在直播";
    } else if ([state isEqualToString:@"3"]) {
        _stateLabel.text = @"正在进行";
    } else if ([state isEqualToString:@"4"]) {
        _stateLabel.text = @"回看";
    } else if ([state isEqualToString:@"1"]) {
        _stateLabel.text = @"尚未开始";
    } else if ([state isEqualToString:@"5"]) {
        _stateLabel.text = @"已结束";
    }

    
    
    
}

- (void)setFrame:(CGRect)frame
{
    CGRect newFrame = frame;
    
    newFrame.origin.x += 5;
    newFrame.size.width -= 10;
    newFrame.origin.y += 2.5;
    newFrame.size.height -= 5;
    
    [super setFrame:newFrame];
}



@end
