//
//  IGNewsTableViewCell1.m
//  iGameApp
//
//  Created by lanou on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGNewsTableViewCell1.h"

@implementation IGNewsTableViewCell1

- (void)awakeFromNib {
 
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(IGNewsListModel *)model
{
    _model = model;
    
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:model.img]];
    _iconImageView.layer.cornerRadius = 5;
    _iconImageView.layer.masksToBounds = YES;
    _contentLabel.text = model.title;
    
}

- (void)setList:(HistoryList *)list
{
    _list = list;
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:list.imgUrlString]];
    _iconImageView.layer.cornerRadius = 5;
    _iconImageView.layer.masksToBounds = YES;
    _contentLabel.text = list.titleName;
}

- (void)setTournamentModel:(IGNewsTournamentModel *)tournamentModel
{
    _tournamentModel = tournamentModel;
    
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:tournamentModel.img]];
    _iconImageView.layer.cornerRadius = 5;
    _iconImageView.layer.masksToBounds = YES;
    _contentLabel.text = tournamentModel.title;

    
}

- (void)setFrame:(CGRect)frame
{
    CGRect newFrame = frame;
    
    newFrame.origin.x += 5;
    newFrame.size.width -= 10;
    newFrame.origin.y += 2.5;
    newFrame.size.height -= 5;

    [super setFrame:newFrame];
}

@end
