//
//  IGNewsTableViewCell5.h
//  iGameApp
//
//  Created by lanou on 16/1/13.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGNewsListModel.h"
@interface IGNewsTableViewCell5 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *icon1Button;

@property (weak, nonatomic) IBOutlet UIButton *icon2Button;

@property (weak, nonatomic) IBOutlet UIButton *icon3Button;

@property (weak, nonatomic) IBOutlet UIButton *icon4Button;


@property (weak, nonatomic) IBOutlet UILabel *title1Label;

@property (weak, nonatomic) IBOutlet UILabel *title2Label;

@property (weak, nonatomic) IBOutlet UILabel *title3Label;

@property (weak, nonatomic) IBOutlet UILabel *title4Label;

@property (nonatomic, strong) IGNewsListModel *model;


@end
