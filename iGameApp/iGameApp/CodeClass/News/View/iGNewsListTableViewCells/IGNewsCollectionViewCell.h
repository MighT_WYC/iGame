//
//  IGNewsCollectionViewCell.h
//  iGameApp
//
//  Created by lanou on 16/1/14.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGNewsCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;


@end
