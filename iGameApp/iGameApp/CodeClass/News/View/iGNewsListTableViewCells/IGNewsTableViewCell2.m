//
//  IGNewsTableViewCell2.m
//  iGameApp
//
//  Created by lanou on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGNewsTableViewCell2.h"

@implementation IGNewsTableViewCell2

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(IGNewsListModel *)model
{
    _model = model;
    
    if (model.img != nil) {
        [_iconImageView sd_setImageWithURL:[NSURL URLWithString:model.img]];
    }else{
        [_iconImageView sd_setImageWithURL:[NSURL URLWithString:model.infoalbum_image]];
    }
    
    _titleLabel.text = model.infoalbum_title;
    
}

- (void)setTournamentModel:(IGNewsTournamentModel *)tournamentModel
{
    _tournamentModel = tournamentModel;
    
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:tournamentModel.image]];

}

- (void)setFrame:(CGRect)frame
{
    CGRect newFrame = frame;
    
    newFrame.origin.x += 5;
    newFrame.size.width -= 10;
    newFrame.origin.y += 2.5;
    newFrame.size.height -= 5;
    
    [super setFrame:newFrame];
}

@end
