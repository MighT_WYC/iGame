//
//  IGNewsTableViewCell4.m
//  iGameApp
//
//  Created by lanou on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGNewsTableViewCell4.h"

@implementation IGNewsTableViewCell4

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(IGNewsListModel *)model
{
    _model = model;
    
    [_icon1Button sd_setBackgroundImageWithURL:[NSURL URLWithString:model.img1] forState:UIControlStateNormal];
    [_icon2Button sd_setBackgroundImageWithURL:[NSURL URLWithString:model.img2] forState:UIControlStateNormal];

    _title1Label.text = model.title1;
    _title2Label.text = model.title2;
    
        
}


- (void)setFrame:(CGRect)frame
{
    CGRect newFrame = frame;
    
    newFrame.origin.x += 5;
    newFrame.size.width -= 10;
    newFrame.origin.y += 2.5;
    newFrame.size.height -= 5;
    
    [super setFrame:newFrame];
}

@end
