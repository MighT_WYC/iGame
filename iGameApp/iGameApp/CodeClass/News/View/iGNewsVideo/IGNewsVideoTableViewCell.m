//
//  IGNewsVideoTableViewCell.m
//  iGameApp
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGNewsVideoTableViewCell.h"

@implementation IGNewsVideoTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(IGNewsVideoModel *)model
{
    
    _model = model;
    
    _titleLabel.text = model.title;
}

- (void)setFrame:(CGRect)frame
{
    CGRect newFrame = frame;
    
    newFrame.origin.x += 5;
    newFrame.size.width -= 10;
    newFrame.origin.y += 2.5;
    newFrame.size.height -= 5;
    
    [super setFrame:newFrame];
}


@end
