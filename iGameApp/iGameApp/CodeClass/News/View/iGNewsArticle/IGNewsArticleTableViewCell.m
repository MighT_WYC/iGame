//
//  IGNewsArticleTableViewCell.m
//  iGameApp
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGNewsArticleTableViewCell.h"

@implementation IGNewsArticleTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(IGNewsArticleModel *)model
{
    _model = model;
    
    _titleLabel.text = model.article_title;
    _timeLabel.text = [self time];
    
}

// 计算时间
- (NSString *)time
{
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
    CGFloat timeInterval = (time - _model.addtime.floatValue) / 3600;
    if (timeInterval > 1) {
        return [NSString stringWithFormat:@"发表于%.0f小时前",timeInterval];
    }else {
        if (timeInterval*60 > 1) {
            return [NSString stringWithFormat:@"发表于%.0f分钟前",timeInterval*60];
        }else{
            return @"发表于1分钟前";
        }
    }
}

- (void)setFrame:(CGRect)frame
{
    CGRect newFrame = frame;
    
    newFrame.origin.x += 5;
    newFrame.size.width -= 10;
    newFrame.origin.y += 2.5;
    newFrame.size.height -= 5;
    
    [super setFrame:newFrame];
}

@end
