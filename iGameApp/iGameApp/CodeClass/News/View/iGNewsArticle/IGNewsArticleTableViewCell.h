//
//  IGNewsArticleTableViewCell.h
//  iGameApp
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGNewsArticleModel.h"

@interface IGNewsArticleTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (nonatomic, strong) IGNewsArticleModel *model;

@end
