//
//  IGNewsTournamentHeader.h
//  iGameApp
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGNewsTournamentHeader : UIView

@property (weak, nonatomic) IBOutlet UIImageView *tournamentImage;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end
