//
//  IG_UserMessage.h
//  iGameApp
//
//  Created by Yf66 on 16/1/19.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IG_UserMessage : NSObject

@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, strong) IG_UserMessage *userMessage;
//@property (nonatomic, strong) NSNumber *uid;
//@property (nonatomic, strong) NSString *userCity;
//@property (nonatomic, strong) NSString *userName;
//@property (nonatomic, strong) NSString *userIcon;
@property (nonatomic, strong) NSDictionary *dic;
@property (nonatomic, strong) NSMutableArray *sublistArray;
+ (instancetype)shareUserMessage;

@end
