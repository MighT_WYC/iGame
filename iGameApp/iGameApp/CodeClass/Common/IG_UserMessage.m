//
//  IG_UserMessage.m
//  iGameApp
//
//  Created by Yf66 on 16/1/19.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_UserMessage.h"


@implementation IG_UserMessage

+ (instancetype)shareUserMessage {
    static dispatch_once_t predicate;
    static IG_UserMessage * userMessage = nil;
    dispatch_once(&predicate, ^{
        userMessage =[[IG_UserMessage alloc] init];
        
    });
    return userMessage;
}

- (instancetype)init {
    self = [super init];
    if (self == nil) {
        _userMessage = [[IG_UserMessage alloc] init];
        _sublistArray = [NSMutableArray array];
        _isLoading = NO;
    }
    return self;
}

@end
