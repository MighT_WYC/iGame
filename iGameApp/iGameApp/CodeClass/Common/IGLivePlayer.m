//
//  IGLivePlayer.m
//  iGameApp
//
//  Created by lanou on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGLivePlayer.h"
@interface IGLivePlayer ()

@property (nonatomic, strong) UIButton *backButton;

@end

@implementation IGLivePlayer

- (instancetype)initWithFrame:(CGRect)frame URL:(NSURL *)url
{
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor blackColor];
        
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        
        _vlcPlayer = [[VLCMediaPlayer alloc] initWithOptions:nil];
        _vlcPlayer.drawable = self;
        VLCMedia *media = [VLCMedia mediaWithURL:url];
        [_vlcPlayer setMedia:media];
        [_vlcPlayer play];
        
    }
    return self;
}




@end
