//
//  IGLoginViewController.m
//  iGameApp
//
//  Created by lanou on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGLoginViewController.h"
#import "IGRegisterViewController.h"
@interface IGLoginViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *userTextField;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;


@end

@implementation IGLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}


- (IBAction)back:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (IBAction)login:(UIButton *)sender {

    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:nil status:@"正在登录中"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"postJson"] = [NSString stringWithFormat:@"%%7B%%22username%%22%%3A%%22%@%%22%%2C%%22password%%22%%3A%%22%@%%22%%2C%%22sub_ids%%22%%3A%%5B7%%5D%%2C%%22device_token%%22%%3A%%22%@%%22%%7D",_userTextField.text,_passwordTextField.text,@""];
    [IGAFNetworking newRequestWithUrlString:@"http://www.imbatv.cn/api_2_4_0/userLogin" parDic:dic method:POST finish:^(NSDictionary *dataDic) {
        NSLog(@"%@",dataDic);
        if ([dataDic[@"message"] isEqualToString:@"登录成功"]) {
            [MMProgressHUD dismissWithSuccess:@"登录成功"];
            [IG_UserMessage shareUserMessage].dic = dataDic[@"data"];
            [IG_UserMessage shareUserMessage].isLoading = YES;
             [[NSNotificationCenter defaultCenter] postNotificationName:@"USER" object:nil userInfo:nil];
            [self dismissViewControllerAnimated:YES completion:nil];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:dataDic[@"data"] forKey:@"userInfo"];
            [defaults setBool:YES forKey:@"isLoading"];
            
            
        }else{
            [MMProgressHUD dismissWithError:@"登录失败"];
        }
    } error:^(NSError *requestError) {
        NSLog(@"%@",requestError);
        [MMProgressHUD dismissWithError:@"登录失败"];
    }];
}

- (void)saveInfo
{
    
    
}

- (IBAction)registerClick:(UIButton *)sender {
    
    IGRegisterViewController *registerVC = [[IGRegisterViewController alloc] init];
       [self presentViewController:registerVC animated:YES completion:nil];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_userTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    
    return YES;
    
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [_userTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
