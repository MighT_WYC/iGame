//
//  IGLoginViewController.h
//  iGameApp
//
//  Created by lanou on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^LoginSuccessBlock)(NSDictionary *);
@interface IGLoginViewController : UIViewController

@property (nonatomic, copy) LoginSuccessBlock loginSuccessBlock;
@end
