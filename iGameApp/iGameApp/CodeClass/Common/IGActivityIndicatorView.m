//
//  IGActivityIndicatorView.m
//  iGameApp
//
//  Created by lanou on 16/1/14.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGActivityIndicatorView.h"

@implementation IGActivityIndicatorView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        CGFloat width = frame.size.width;
        CGFloat height = frame.size.height;
        
        UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityIndicatorView.frame = CGRectMake(0, 0, width / 2, height);
        
        
        
    }
    return self;
}


@end
