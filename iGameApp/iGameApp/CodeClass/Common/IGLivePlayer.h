//
//  IGLivePlayer.h
//  iGameApp
//
//  Created by lanou on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileVLCKit/MobileVLCKit.h>
@interface IGLivePlayer : UIView

- (instancetype)initWithFrame:(CGRect)frame URL:(NSURL *)url;

@property (nonatomic, strong) UIButton *button;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) VLCMediaPlayer *vlcPlayer;

@end
