//
//  IGMap.m
//  iGameApp
//
//  Created by lanou on 16/1/27.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGMap.h"

@interface IGMap () <CLLocationManagerDelegate>
@property (nonatomic, strong) CLGeocoder *geocoder;


@end

@implementation IGMap


- (CLGeocoder *)geocoder {
    if (_geocoder == nil) {
        _geocoder = [[CLGeocoder alloc] init];
    }
    return _geocoder;
}

- (CLLocationManager *)locationManager {
    if (_locationManager == nil) {
        _locationManager = [[CLLocationManager alloc] init];
        // 创建同时设置代理为self
        _locationManager.delegate = self;
    }
    return _locationManager;
}


+ (id)shareLocationCityName
{
    static IGMap *cityName = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cityName = [[IGMap alloc] init];
    });
    return cityName;
}

- (void)openMap
{
    if (![CLLocationManager locationServicesEnabled]) {
        NSLog(@"定位服务未开启");
        return;
    }
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestAlwaysAuthorization];
    }else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse){
        if ([[[UIDevice currentDevice] systemVersion] doubleValue] > 8.0) {
            [self.locationManager requestAlwaysAuthorization];
        }
        
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = kCLDistanceFilterNone;
    }
    _cityName = @"未知";
    [self.locationManager requestLocation];
}

#pragma mark ------ 代理方法 ------ 不断触发
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *location = [locations firstObject];

    NSLog(@"%@", location);
    NSString *latitudeStr = [NSString stringWithFormat:@"%.2lf", location.coordinate.latitude];
    NSString *longitudeStr = [NSString stringWithFormat:@"%.2lf", location.coordinate.longitude];

  [self getCityNameWithLatitude:latitudeStr longitude:longitudeStr];
   
}

// 返回错误信息
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"%@", error);
}



#pragma mark ------ 地理编码相关
// 反地理编码: 经纬度坐标->地名
- (void)getCityNameWithLatitude:(NSString *)latitudeStr longitude:(NSString *)longitudeStr
{
    if (latitudeStr.length == 0 || longitudeStr.length == 0) {
        _cityName = @"未知";
        return;
    }
    CLLocationDegrees latitude = [latitudeStr doubleValue];
    CLLocationDegrees longitude = [longitudeStr doubleValue];
    // 根据经纬度创建location对象.
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    [self.geocoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        
        if (error || placemarks.count == 0) {
            _cityName = @"未知";
        } else {
            // 编码成功
            CLPlacemark *placemark = [placemarks firstObject];
            _cityName = placemark.locality;
        }
    }];
}

@end
