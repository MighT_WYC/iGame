//
//  IGMap.h
//  iGameApp
//
//  Created by lanou on 16/1/27.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
@interface IGMap : NSObject

+ (id)shareLocationCityName;
- (void)openMap;
@property (nonatomic, strong) CLLocationManager *locationManager;
- (void)getCityNameWithLatitude:(NSString *)latitudeStr longitude:(NSString *)longitudeStr;
@property (nonatomic, strong)  NSString *cityName;

@end
