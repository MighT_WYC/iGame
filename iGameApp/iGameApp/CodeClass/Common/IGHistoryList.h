//
//  IGHistoryList.h
//  iGameApp
//
//  Created by lanou on 16/1/20.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGHistoryList : NSObject

+ (IGHistoryList *)defaultIGHistoryList;

- (void)saveDataAtLoacllyWithTitle:(NSString *)title img:(NSString *)img vid:(NSString *)vid;

- (NSMutableArray *)searchLocationData;

- (void)removeAllHistoryList;

@end
