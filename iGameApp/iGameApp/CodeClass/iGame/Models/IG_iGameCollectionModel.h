//
//  IG_iGameCollectionModel.h
//  iGameApp
//
//  Created by Yf66 on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IG_iGameCollectionModel : NSObject

@property (nonatomic, strong) NSString *album_idStr;
@property (nonatomic, strong) NSString *album_image;
@property (nonatomic, strong) NSString *album_image_vertical;
@property (nonatomic, strong) NSString *album_name;
@property (nonatomic, strong) NSString *video_idStr;

@end
