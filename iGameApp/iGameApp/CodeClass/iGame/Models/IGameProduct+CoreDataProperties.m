//
//  IGameProduct+CoreDataProperties.m
//  
//
//  Created by Yf66 on 16/1/13.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "IGameProduct+CoreDataProperties.h"

@implementation IGameProduct (CoreDataProperties)

@dynamic album_app_image;
@dynamic album_cate_id;
@dynamic album_cate_name;

@end
