//
//  IGameProduct+CoreDataProperties.h
//  
//
//  Created by Yf66 on 16/1/13.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "IGameProduct.h"

NS_ASSUME_NONNULL_BEGIN

@interface IGameProduct (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *album_app_image;
@property (nullable, nonatomic, retain) NSNumber *album_cate_id;
@property (nullable, nonatomic, retain) NSString *album_cate_name;

@end

NS_ASSUME_NONNULL_END
