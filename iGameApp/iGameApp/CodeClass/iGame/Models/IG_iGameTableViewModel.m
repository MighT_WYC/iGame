//
//  IG_iGameTableViewModel.m
//  iGameApp
//
//  Created by Yf66 on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_iGameTableViewModel.h"

@implementation IG_iGameTableViewModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"time"]) {
        _timeStr = [NSString stringWithFormat:@"%@",value];
    }
    if ([key isEqualToString:@"vid"]) {
        _vidStr = [NSString stringWithFormat:@"%@",value];
    }
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

@end
