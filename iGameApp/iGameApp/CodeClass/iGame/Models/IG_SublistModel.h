//
//  IG_SublistModel.h
//  iGameApp
//
//  Created by Yf66 on 16/1/19.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IG_SublistModel : NSObject
@property (nonatomic, strong) NSNumber *sub_id;
@property (nonatomic, strong) NSString *sub_name;
@end
