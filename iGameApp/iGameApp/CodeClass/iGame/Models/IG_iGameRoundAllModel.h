//
//  IG_iGameRoundAllModel.h
//  iGameApp
//
//  Created by Yf66 on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IG_iGameRoundAllModel : NSObject

@property (nonatomic, strong) NSString *addtimeStr;
@property (nonatomic, strong) NSString *cookie;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *furl;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *player_url;
@property (nonatomic, strong) NSString *share_url;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *vidStr;
@property (nonatomic, strong) NSString *vkey;
@property (nonatomic, strong) NSString *vurl;

@end
