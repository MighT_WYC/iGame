//
//  IG_iGameCollectionModel.m
//  iGameApp
//
//  Created by Yf66 on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_iGameCollectionModel.h"

@implementation IG_iGameCollectionModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"album_id"]) {
        _album_idStr = [NSString stringWithFormat:@"%@",value];
    }
    if ([key isEqualToString:@"video_id"]) {
        _video_idStr = [NSString stringWithFormat:@"%@",value];
    }
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}
@end
