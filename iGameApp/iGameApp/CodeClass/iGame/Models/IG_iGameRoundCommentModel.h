//
//  IG_iGameRoundCommentModel.h
//  iGameApp
//
//  Created by Yf66 on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IG_iGameRoundCommentModel : NSObject

@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSNumber *myID;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSNumber *parent_id;
@property (nonatomic, strong) NSString *timeStr;
@property (nonatomic, strong) NSNumber *uid;
@property (nonatomic, strong) NSString *user_img;
@property (nonatomic, strong) NSNumber *video_id;
@property (nonatomic, strong) NSNumber *top;
@end
