//
//  IG_iGameDetailTableModel.h
//  iGameApp
//
//  Created by Yf66 on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IG_iGameDetailTableModel : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *vidStr;

@end
