//
//  IG_iGameTableViewModel.h
//  iGameApp
//
//  Created by Yf66 on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IG_iGameTableViewModel : NSObject

@property (nonatomic, strong) NSString *addtime;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *timeStr;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *vidStr;

@end
