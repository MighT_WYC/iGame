//
//  IGameProduct.h
//  
//
//  Created by Yf66 on 16/1/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface IGameProduct : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "IGameProduct+CoreDataProperties.h"
