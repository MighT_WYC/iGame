//
//  IG_iGameRoundModel.m
//  iGameApp
//
//  Created by Yf66 on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_iGameRoundModel.h"

@implementation IG_iGameRoundModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"vid"]) {
        _vidStr = [NSString stringWithFormat:@"%@",value];
    }
}

@end
