//
//  IG_iGameRoundModel.h
//  iGameApp
//
//  Created by Yf66 on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IG_iGameRoundModel : NSObject

@property (nonatomic, strong) NSString *img;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *vidStr;

@end
