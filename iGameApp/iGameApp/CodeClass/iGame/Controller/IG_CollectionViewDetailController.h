//
//  IG_CollectionViewDetailController.h
//  iGameApp
//
//  Created by Yf66 on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IG_iGameTableViewCell.h"
#import "IG_iGameTableViewModel.h"
#import "IGRightTabBarController.h"
#import "IG_RoundDetailViewController.h"
@interface IG_CollectionViewDetailController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger requestStart;
@property (nonatomic, strong) NSString *album_id;
@end
