//
//  IGiGameViewController.h
//  iGameApp
//
//  Created by lanou on 16/1/9.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGCustomViewController.h"
#import "IG_headerView.h"
#import "IG_iGameCollectionViewCell.h"
#import "IG_iGameTableViewCell.h"
#import "IG_iGameRoundModel.h"
#import "IG_iGameCollectionModel.h"
#import "IG_iGameTableViewModel.h"
#import "IG_RoundDetailViewController.h"
#import "IG_CollectionViewDetailController.h"
#import "IGRightTabBarController.h"
#import "IG_iGameMoreViewController.h"
#import "IGCustomNavigationController.h"
@interface IGiGameViewController : IGCustomViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IG_headerView *headerView;
@property (nonatomic, strong) NSMutableArray *roundDataArray;
@property (nonatomic, strong) NSMutableArray *collectionDataArray;
@property (nonatomic, strong) NSMutableArray *tableViewDataArray;
@property (nonatomic, assign) NSInteger requestStart;

@end
