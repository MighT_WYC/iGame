//
//  IG_CollectionViewDetailController.m
//  iGameApp
//
//  Created by Yf66 on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_CollectionViewDetailController.h"
#import "IGFindRadioPlayViewController.h"
@interface IG_CollectionViewDetailController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation IG_CollectionViewDetailController

- (void)viewWillAppear:(BOOL)animated {
    IGRightTabBarController *tabBarController = (IGRightTabBarController *)self.navigationController.parentViewController;
    tabBarController.tabBar.hidden = YES;
    IGCustomNavigationController *navC = (IGCustomNavigationController *)self.navigationController;
    navC.iconButton.hidden = YES;
    navC.backButton.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    IGCustomNavigationController *navC = (IGCustomNavigationController *)self.navigationController;
    navC.backButton.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _requestStart = 0;
    // Do any additional setup after loading the view from its nib.
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setUpData];
    [_tableView registerNib:[UINib nibWithNibName:@"IG_iGameTableViewCell" bundle:nil] forCellReuseIdentifier:@"MyTableViewCell"];
    _tableView.rowHeight = 80;
    [self MJRefreshing];
}

- (void)setUpData {
    NSMutableDictionary  *parDic = [NSMutableDictionary dictionary];
    parDic[@"start"] = [NSString stringWithFormat:@"%ld",_requestStart];
    parDic[@"num"] = @"10";
    NSString *newUrl = [NSString stringWithFormat:@"http://www.imbatv.cn/api_2_4_0/getVideoListByAlbum?album_id=%@",_album_id];
    [IGAFNetworking requestWithUrlString:newUrl parDic:parDic metho:GET finish:^(NSDictionary *dataDic) {
        NSArray *array = dataDic[@"data"];
        if (_requestStart == 0) {
            [_dataArray removeAllObjects];
        }
        if (array.count != 0) {
            _dataArray = [NSMutableArray array];
            for (NSDictionary *dic in array) {
                IG_iGameTableViewModel *model = [[IG_iGameTableViewModel alloc] init];
                [model setValuesForKeysWithDictionary:dic];
                [_dataArray addObject:model];
            }
            [_tableView.mj_header endRefreshing];
            [_tableView reloadData];
            
        }
    } error:^(NSError *requestError) {
        NSLog(@"请求失败");
    }];
}

//  上下拉刷新
- (void)MJRefreshing {

    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
         _requestStart = 0;
        [self setUpData];
    }];
}

#pragma mark -- tableview协议方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IG_iGameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyTableViewCell" forIndexPath:indexPath];
    cell.model = _dataArray[indexPath.row];
    cell.backgroundColor = [UIColor whiteColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    IGFindRadioPlayViewController *detailVC = [[IGFindRadioPlayViewController alloc] init];
    IG_iGameTableViewModel *model = _dataArray[indexPath.row];
    [[IGHistoryList defaultIGHistoryList] saveDataAtLoacllyWithTitle:model.title img:model.image vid:model.vidStr];
    detailVC.isFindPushHere = YES;
    detailVC.vid = model.vidStr;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
