//
//  IG_iGameMoreViewController.h
//  iGameApp
//
//  Created by Yf66 on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGRightTabBarController.h"
#import "IGameProduct+CoreDataProperties.h"
#import "IG_iGameCollectionViewCell.h"
#import "IG_iGameCollectionModel.h"
#import "IG_CollectionViewDetailController.h"
@interface IG_iGameMoreViewController : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) NSInteger requestStart;
@property (nonatomic, strong) NSMutableArray *nDataArray;

@end
