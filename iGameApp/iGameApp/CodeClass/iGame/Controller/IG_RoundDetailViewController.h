//
//  IG_RoundDetailViewController.h
//  iGameApp
//
//  Created by Yf66 on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "IG_iGameRoundAllModel.h"
#import "IG_iGameRoundHeaderView.h"
#import "IG_iGameDetailTableModel.h"
#import "IG_iGameRoundTableViewCell.h"
#import "IG_RoundCommentTableViewCell.h"
#import "IG_iGameRoundCommentModel.h"
#import "IGRightTabBarController.h"
#import "IGFindCommentModel.h"
#import "IGLoginViewController.h"
#import "IG_SublistModel.h"

@interface IG_RoundDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IG_iGameRoundAllModel *allModel;
@property (nonatomic, strong) NSString *vid;
@property (nonatomic, strong) IG_iGameRoundHeaderView *headerView;
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) AVPlayerItem *playerItem;
@property (nonatomic, strong) UIView *playView;
@property (nonatomic, strong) NSMutableArray *tableViewDataArray;
@property (nonatomic, strong) NSMutableArray *commentDataArray;
@property (nonatomic, assign) NSInteger requestStart;
@property (nonatomic, assign) NSInteger count;
@property (nonatomic, assign) NSInteger tableViewCount;
@property (weak, nonatomic) IBOutlet UITextField *commentTF;
@property (weak, nonatomic) IBOutlet UIButton *commentCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentIconBtn;
@property (nonatomic, strong) NSMutableArray *commentArray;
@property (nonatomic, strong) NSMutableArray *userSublistArray;
@property (nonatomic, assign) BOOL isComment;
@end
