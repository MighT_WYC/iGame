//
//  IG_headerView.m
//  iGameApp
//
//  Created by Yf66 on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_headerView.h"

@implementation IG_headerView

- (void)setScrollView:(UIScrollView *)scrollView {
    _scrollView = scrollView;
    for (int i = 0; i < 3; i++) {
        UIButton *button = [UIButton buttonWithType:(UIButtonTypeCustom)];
        button.frame = CGRectMake(kScreenWidth * i, 0, kScreenWidth, scrollView.frame.size.height);
        button.tag = 100 + i;
        button.backgroundColor = [UIColor blackColor];
        [scrollView addSubview:button];
    }
    _firstBtn = [scrollView viewWithTag:100];
    _secondBtn = [scrollView viewWithTag:101];
    _thirdBtn = [scrollView viewWithTag:102];
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake((kScreenWidth - 0) / 2, 160, 40, 20)];
    _pageControl.numberOfPages = 2;
    _pageControl.currentPage = 0;
    [self addSubview:_pageControl];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
