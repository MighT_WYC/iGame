//
//  IG_iGameCollectionViewCell.h
//  iGameApp
//
//  Created by Yf66 on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IG_iGameCollectionModel.h"
#import "IGameProduct+CoreDataProperties.h"
@interface IG_iGameCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (nonatomic, strong) IG_iGameCollectionModel *model;
@property (nonatomic, strong) IGameProduct *product;
@end
