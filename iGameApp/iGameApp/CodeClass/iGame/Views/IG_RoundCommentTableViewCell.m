//
//  IG_RoundCommentTableViewCell.m
//  iGameApp
//
//  Created by Yf66 on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_RoundCommentTableViewCell.h"

@implementation IG_RoundCommentTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// 计算时间
- (NSString *)time
{
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
    CGFloat timeInterval = (time - _model.timeStr.floatValue) / 3600;
    if (timeInterval > 1) {
        if (timeInterval > 24) {
            if (timeInterval >= 24 && timeInterval <= 48) {
                return @"发表于昨天";
            } else {
                return [NSString stringWithFormat:@"发表于%.0f天前",timeInterval / 24];
            }
        } else {
            return [NSString stringWithFormat:@"发表于%.0f小时前",timeInterval];
        }
    }else {
        if (timeInterval*60 > 1) {
            return [NSString stringWithFormat:@"发表于%.0f分钟前",timeInterval*60];
        }else{
            return @"发表于1分钟前";
        }
    }
}

- (void)setModel:(IG_iGameRoundCommentModel *)model {
    _model = model;
    if (model.user_img == nil || [model.user_img isEqualToString:@""]) {
        _photoImage.image = nil;
        _photoImage.backgroundColor = [UIColor grayColor];
    } else {
        [_photoImage sd_setImageWithURL:[NSURL URLWithString:model.user_img] placeholderImage:[UIImage imageNamed:@"bg"]];
    }
    NSString *timestr = [self time];
    _nameLabel.text = model.nickname;
    _timeLabel.text = [NSString stringWithFormat:@"%@  %@",_model.city, timestr];
    _commentLabel.text = model.content;
    _commentCountLabel.text = [NSString stringWithFormat:@"%@",model.top];
}

- (void)setFrame:(CGRect)frame
{
    CGRect newFrame = frame;
    
    newFrame.origin.x += 5;
    newFrame.size.width -= 10;
    newFrame.origin.y += 2.5;
    newFrame.size.height -= 5;
    
    [super setFrame:newFrame];
}


@end
