//
//  IG_iGameTableViewCell.h
//  iGameApp
//
//  Created by Yf66 on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IG_iGameTableViewModel.h"
@interface IG_iGameTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (nonatomic, strong) IG_iGameTableViewModel *model;
@end
