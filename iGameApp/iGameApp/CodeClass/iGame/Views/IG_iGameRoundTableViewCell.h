//
//  IG_iGameRoundTableViewCell.h
//  iGameApp
//
//  Created by Yf66 on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IG_iGameDetailTableModel.h"
@interface IG_iGameRoundTableViewCell : UITableViewCell

@property (nonatomic, strong) IG_iGameDetailTableModel *model;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
