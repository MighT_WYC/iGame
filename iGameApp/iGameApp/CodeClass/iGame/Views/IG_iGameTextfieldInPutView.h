//
//  IG_iGameTextfieldInPutView.h
//  iGameApp
//
//  Created by Yf66 on 16/1/18.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IG_iGameTextfieldInPutView : UIView

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet UITextView *commentTV;

@end
