//
//  IG_iGameRoundTableViewCell.m
//  iGameApp
//
//  Created by Yf66 on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_iGameRoundTableViewCell.h"

@implementation IG_iGameRoundTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(IG_iGameDetailTableModel *)model {
    _model = model;
    _titleLabel.text = model.title;
}

@end
