//
//  IG_iGameCollectionViewCell.m
//  iGameApp
//
//  Created by Yf66 on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_iGameCollectionViewCell.h"

@implementation IG_iGameCollectionViewCell


- (void)awakeFromNib {
    // Initialization code
}

- (void)setModel:(IG_iGameCollectionModel *)model {
    _model = model;
    [_photoView sd_setImageWithURL:[NSURL URLWithString:model.album_image_vertical] placeholderImage:[UIImage imageNamed:@"bg"]];
    _nameLabel.text = model.album_name;
    [_nameLabel adjustsFontSizeToFitWidth];
}

- (void)setProduct:(IGameProduct *)product {
    _product = product;
    [_photoView sd_setImageWithURL:[NSURL URLWithString:product.album_app_image] placeholderImage:[UIImage imageNamed:@"bg"]];
    _nameLabel.text = product.album_cate_name;
}



@end
