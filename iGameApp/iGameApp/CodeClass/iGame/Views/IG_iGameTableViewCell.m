//
//  IG_iGameTableViewCell.m
//  iGameApp
//
//  Created by Yf66 on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IG_iGameTableViewCell.h"

@implementation IG_iGameTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(IG_iGameTableViewModel *)model {
    _model = model;
    [_tableView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"bg"]];
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"yyyy年MM月dd日 HH:mm:ss"];
//    NSString *dateLoca = [NSString stringWithFormat:@"%@",model.timeStr];
//    NSTimeInterval time=[dateLoca doubleValue];
//    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
//    NSString *timestr = [formatter stringFromDate:detaildate];
    _nameLabel.text = model.title;
    _timeLabel.text = model.addtime;
}

@end
