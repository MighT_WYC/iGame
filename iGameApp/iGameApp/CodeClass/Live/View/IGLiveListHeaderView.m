//
//  IGLiveListHeaderView.m
//  iGameApp
//
//  Created by lanou on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGLiveListHeaderView.h"

@implementation IGLiveListHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        
        UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 7.5, 3, 15)];
        lineLabel.backgroundColor = [UIColor blackColor];
        [self addSubview:lineLabel];
        
        UILabel *namelabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 10)];
        namelabel.font = [UIFont systemFontOfSize:12];
        namelabel.text = @"直播列表";
        namelabel.textColor = [UIColor whiteColor];
        [self addSubview:namelabel];

    }
    return self;
    
}



@end
