//
//  IGLiveList2TableViewCell.m
//  iGameApp
//
//  Created by lanou on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGLiveList2TableViewCell.h"

@implementation IGLiveList2TableViewCell

- (void)setModel:(IGLiveListModel *)model
{
    _model = model;
    
    [_bgImageView sd_setImageWithURL:[NSURL URLWithString:_model.live_img]];
    _titleLabel.text = _model.live_title;
    _nameLabel.text = _model.live_nickname;
    _numberLabel.text = [NSString stringWithFormat:@"%@名观众",_model.live_online];
    [_uiconImageView sd_setImageWithURL:[NSURL URLWithString:_model.live_userimg]];
    _uiconImageView.layer.cornerRadius = 20;
    _uiconImageView.layer.masksToBounds = YES;
    
}

- (void)setFrame:(CGRect)frame
{
    CGRect newFrame = frame;
    
    newFrame.origin.x += 5;
    newFrame.size.width -= 10;
    newFrame.origin.y += 2.5;
    newFrame.size.height -= 5;
    
    [super setFrame:newFrame];
    
    
}



- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
