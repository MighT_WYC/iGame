//
//  IGLiveListModel.h
//  iGameApp
//
//  Created by lanou on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGLiveListModel : NSObject

@property (nonatomic, strong) NSString *live_img;

@property (nonatomic, strong) NSString *live_nickname;

@property (nonatomic, strong) NSNumber *live_online;

@property (nonatomic, strong) NSString *live_title;

@property (nonatomic, strong) NSString *live_userimg;

@property (nonatomic, strong) NSString *live_id;

@property (nonatomic, strong) NSString *live_type;

@end
