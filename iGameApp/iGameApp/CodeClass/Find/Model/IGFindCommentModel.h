//
//  IGFindCommentModel.h
//  iGameApp
//
//  Created by lanou on 16/1/13.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGFindCommentModel : NSObject

@property (nonatomic, strong) NSString *commentID;
@property (nonatomic, strong) NSString *parent_id;
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *top;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *is_delete;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *user_img;
@property (nonatomic, strong) NSString *is_top;


/*
 "id": "107",
 "parent_id": "0",
 "uid": "348431",
 "video_id": "9185",
 "content": "。",
 "time": "1452677431",
 "top": "0",
 "city": "上海市",
 "is_delete": "0",
 "nickname": "ssss",
 "user_img": "",
 "is_top": -1
 
 */
@end
