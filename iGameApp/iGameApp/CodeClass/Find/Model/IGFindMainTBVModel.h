//
//  IGFindMainTBVModel.h
//  iGameApp
//
//  Created by lanou on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGFindMainTBVModel : NSObject
@property (nonatomic, strong) NSString *addtime;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *vid;

@end
