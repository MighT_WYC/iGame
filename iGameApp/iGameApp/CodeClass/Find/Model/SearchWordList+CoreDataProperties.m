//
//  SearchWordList+CoreDataProperties.m
//  iGameApp
//
//  Created by lanou on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SearchWordList+CoreDataProperties.h"

@implementation SearchWordList (CoreDataProperties)

@dynamic searchWord;

@end
