//
//  SearchWordList+CoreDataProperties.h
//  iGameApp
//
//  Created by lanou on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SearchWordList.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchWordList (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *searchWord;

@end

NS_ASSUME_NONNULL_END
