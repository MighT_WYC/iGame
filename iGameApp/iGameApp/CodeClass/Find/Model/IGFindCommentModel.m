//
//  IGFindCommentModel.m
//  iGameApp
//
//  Created by lanou on 16/1/13.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGFindCommentModel.h"

@implementation IGFindCommentModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        _commentID = value;
    }
}

@end
