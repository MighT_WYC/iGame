//
//  IGFindSearchContentTBVController.h
//  iGameApp
//
//  Created by lanou on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, WhereToHere) {
    SearchVCPush,
    MainMiddleButton
};


@interface IGFindSearchContentTBVController : UITableViewController
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSString *titleName;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, assign) WhereToHere pushHere;

- (void)addTableViewHeadView;

@end
