//
//  IGFindSearchContentTBVController.m
//  iGameApp
//
//  Created by lanou on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGFindSearchContentTBVController.h"
#import "IGFindMainTBVModel.h"
#import "IGFindMainTableViewCell.h"
#import "IGFindRadioPlayViewController.h"

@interface IGFindSearchContentTBVController ()
@property (nonatomic, strong) IGCustomNavigationController *navc;

@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UILabel *label;
@end

@implementation IGFindSearchContentTBVController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // 共用主页面的cell
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.tableView registerNib:[UINib nibWithNibName:@"IGFindMainTableViewCell" bundle:nil] forCellReuseIdentifier:@"myCell"];
    self.tableView.rowHeight = 93;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // 解决push后侧滑返回失效问题
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
}

- (void)viewDidLayoutSubviews
{
    self.tableView.frame = CGRectMake(0, 64, kScreenWidth, kScreenHeight - 64);
}


// 如果没有数据, 显示表头
- (void)addTableViewHeadView
{
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
    _titleLabel.font = [UIFont systemFontOfSize:25];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.text = @"暂无数据";
    self.tableView.tableHeaderView = _titleLabel;
    self.tableView.tableFooterView = [[UIView alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    UISearchBar *searchBar = (UISearchBar *)[_navc.bgView viewWithTag:1000];
    _navc = (IGCustomNavigationController *)self.navigationController;
    if (_pushHere == MainMiddleButton) {
        searchBar.hidden = YES;
        _navc.iconButton.hidden = YES;
        _button = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _button.frame = CGRectMake(10, 10, 50, 25);
        [_button setTitle:@"返回" forState:(UIControlStateNormal)];
        [_button setTitleColor:[UIColor blueColor] forState:(UIControlStateNormal)];
        [_button addTarget:self action:@selector(buttonClick:) forControlEvents:(UIControlEventTouchUpInside)];
        [_navc.bgView addSubview:_button];
        _label = [[UILabel alloc] initWithFrame:CGRectMake(kScreenWidth/2 - 50, 10, 100, 34)];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.text = _titleName;
        _label.textColor = [UIColor blueColor];
        [_navc.bgView addSubview:_label];
    }else{
        searchBar.hidden = NO;
    }
}

- (void)buttonClick:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (_pushHere == MainMiddleButton) {
        _navc.iconButton.hidden = YES;
        [_button removeFromSuperview];
        [_label removeFromSuperview];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
    return _dataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IGFindMainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
    IGFindMainTBVModel *model = _dataArray[indexPath.row];
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IGFindRadioPlayViewController *movieVC = [[IGFindRadioPlayViewController alloc] init];
    IGFindMainTBVModel *model = _dataArray[indexPath.row];
    movieVC.vid = model.vid;
    movieVC.isFindPushHere = YES;
    [self.navigationController pushViewController:movieVC animated:YES];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
