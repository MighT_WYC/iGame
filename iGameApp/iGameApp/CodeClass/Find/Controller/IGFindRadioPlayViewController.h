//
//  IGFindRadioPlayViewController.h
//  iGameApp
//
//  Created by lanou on 16/1/12.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGFindRadioPlayViewController : UIViewController
@property (nonatomic, strong) NSString *vid;
@property (nonatomic, assign) BOOL isFindPushHere;

@end
