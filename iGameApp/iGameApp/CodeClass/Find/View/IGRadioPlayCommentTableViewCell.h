//
//  IGRadioPlayCommentTableViewCell.h
//  iGameApp
//
//  Created by lanou on 16/1/13.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGFindCommentModel.h"

@interface IGRadioPlayCommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *sayGoodLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentContentLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userIconImageView;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (nonatomic, strong) IGFindCommentModel *model;

@end
