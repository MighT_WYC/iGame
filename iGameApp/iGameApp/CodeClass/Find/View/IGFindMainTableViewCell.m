//
//  IGFindMainTableViewCell.m
//  iGameApp
//
//  Created by lanou on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGFindMainTableViewCell.h"

@implementation IGFindMainTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(IGFindMainTBVModel *)model
{
    _model = model;
    
    [_coverImg sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:nil];
    _titleLabel.text = model.title;
    _dateLabel.text = model.addtime;
}

@end
