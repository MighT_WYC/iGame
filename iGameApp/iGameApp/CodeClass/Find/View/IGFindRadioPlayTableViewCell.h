//
//  IGFindRadioPlayTableViewCell.h
//  iGameApp
//
//  Created by lanou on 16/1/13.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGFindRadioPlayTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
