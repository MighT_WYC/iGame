//
//  ss_customSearchBar.m
//  tttttttt
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 zft. All rights reserved.
//

#import "ss_customSearchBar.h"

@implementation ss_customSearchBar

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _textView = [[UITextField alloc] initWithFrame:CGRectMake(5, 4, frame.size.width - 65, frame.size.height - 11)];
        _textView.backgroundColor = [UIColor whiteColor];
        _textView.placeholder = @"                        搜索";
        _textView.layer.cornerRadius = 5;
        _textView.font = [UIFont systemFontOfSize:12];
        _textView.returnKeyType = UIReturnKeySearch;
        [self addSubview:_textView];
        
        _button = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _button.frame = CGRectMake(frame.size.width - 50, 4, 40, frame.size.height - 11);
        _button.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:.2];
        [_button setTitle:@"取消" forState:(UIControlStateNormal)];
        _button.layer.cornerRadius = 5;
        _button.titleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_button];
        
        _searchButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _searchButton.frame = CGRectMake(_button.frame.origin.x - 30, 4, 25, frame.size.height - 11);
        _searchButton.layer.cornerRadius = 10;
        _searchButton.layer.masksToBounds = 10;
        _searchButton.backgroundColor = [UIColor whiteColor];
        [_searchButton setBackgroundImage:[UIImage imageNamed:@"searchBar"] forState:(UIControlStateNormal)];
        [self addSubview:_searchButton];
        
    }
    return self;
}

@end
