//
//  IGFindCommentView.m
//  iGameApp
//
//  Created by lanou on 16/1/13.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGFindCommentView.h"

@implementation IGFindCommentView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _commentTextFiled = [[UITextField alloc] initWithFrame:CGRectMake(8, 8, kScreenWidth - 80, 28)];
        _commentTextFiled.borderStyle = UITextBorderStyleRoundedRect;
        _commentTextFiled.placeholder = @"我要说两句";
        _commentTextFiled.font = [UIFont systemFontOfSize:12];
        [self addSubview:_commentTextFiled];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(kScreenWidth - 64, 8, 56, _commentTextFiled.frame.size.height)];
        view.backgroundColor = [UIColor grayColor];
        view.layer.cornerRadius = 5;
        [self addSubview:view];
        
        _commentCountButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _commentCountButton.frame = CGRectMake(0, 0, view.frame.size.width/2, view.frame.size.height);
        _commentCountButton.backgroundColor = [UIColor grayColor];
        _commentCountButton.layer.cornerRadius = 5;
        _commentCountButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_commentCountButton setTitleColor:[UIColor yellowColor] forState:(UIControlStateNormal)];
        [_commentCountButton setTitle:@"沙发" forState:(UIControlStateNormal)];
        [view addSubview:_commentCountButton];
        
        _commentIconButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _commentIconButton.frame = CGRectMake(view.frame.size.width/2, 2, view.frame.size.width/2-3, view.frame.size.height-4);
        _commentIconButton.backgroundColor = [UIColor grayColor];
        _commentIconButton.layer.cornerRadius = 5;
        [_commentIconButton setBackgroundImage:[UIImage imageNamed:@"iconfont-iconfontshafa"] forState:(UIControlStateNormal)];
        [view addSubview:_commentIconButton];
        
        _writeCommentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 150)];
        _commentTextFiled.inputAccessoryView = _writeCommentView;
        
        _writeCommentTV = [[UITextView alloc] initWithFrame:CGRectMake(10, 50, kScreenWidth - 20, 90)];
        _writeCommentTV.backgroundColor = [UIColor whiteColor];
        [_writeCommentView addSubview:_writeCommentTV];
        
        _cancerButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _cancerButton.frame = CGRectMake(15, 10, 50, 30);
        [_cancerButton
         setTitle:@"取消" forState:(UIControlStateNormal)];
        [_cancerButton setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
        _cancerButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_writeCommentView addSubview:_cancerButton];
        
        _sendButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _sendButton.frame = CGRectMake(kScreenWidth - 65, 10, 50, 30);
        [_sendButton setTitle:@"发送" forState:(UIControlStateNormal)];
        [_sendButton setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
        _sendButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_writeCommentView addSubview:_sendButton];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kScreenWidth/2-30, 10, 60, 30)];
        label.text = @"写评论";
        label.textColor = [UIColor blackColor];
        [_writeCommentView addSubview:label];
 
    }
    return self;
}

@end
