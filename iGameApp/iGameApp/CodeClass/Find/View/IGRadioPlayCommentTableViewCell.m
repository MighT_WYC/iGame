//
//  IGRadioPlayCommentTableViewCell.m
//  iGameApp
//
//  Created by lanou on 16/1/13.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import "IGRadioPlayCommentTableViewCell.h"

@implementation IGRadioPlayCommentTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)deleteAction:(id)sender {
    
}
/*

 "id": "107",
 "parent_id": "0",
 "uid": "348431",
 "video_id": "9185",
 "content": "。",
 "time": "1452677431",
 "top": "0",
 "city": "上海市",
 "is_delete": "0",
 "nickname": "ssss",
 "user_img": "",
 "is_top": -1
 

 @property (weak, nonatomic) IBOutlet UILabel *sayGoodLabel;
 @property (weak, nonatomic) IBOutlet UILabel *timeLabel;
 @property (weak, nonatomic) IBOutlet UILabel *commentContentLabel;
 @property (weak, nonatomic) IBOutlet UILabel *cityLabel;
 @property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
 @property (weak, nonatomic) IBOutlet UIImageView *userIconImageView;
 @property (weak, nonatomic) IBOutlet UIButton *deleteButton;
 */
- (void)setModel:(IGFindCommentModel *)model
{
    _model = model;
    
    _commentContentLabel.text = _model.content;
    if ([_model.city isEqualToString:@""]) {
        _model.city = @"未知";
    }
    _cityLabel.text = _model.city;
    _userNameLabel.text = _model.nickname;
    [_userIconImageView sd_setImageWithURL:[NSURL URLWithString:_model.user_img] placeholderImage:nil];
    _timeLabel.text = [self time];
}

// 计算时间
- (NSString *)time
{
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
    CGFloat timeInterval = (time - _model.time.floatValue) / 3600;
    
    if (timeInterval > 24) {
        return [NSString stringWithFormat:@"发表于%.0f天前",timeInterval/24];
    }else if (timeInterval > 1) {
        return [NSString stringWithFormat:@"发表于%.0f小时前",timeInterval];
    }else {
        if (timeInterval*60 > 1) {
            return [NSString stringWithFormat:@"发表于%.0f分钟前",timeInterval*60];
        }else{
        return [NSString stringWithFormat:@"发表于%.0f秒前",timeInterval*60*60+25];
        }
    }
}

@end
