//
//  ss_customSearchBar.h
//  tttttttt
//
//  Created by lanou on 16/1/16.
//  Copyright © 2016年 zft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ss_customSearchBar : UIView

@property (nonatomic, strong) UITextField *textView;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UIButton *searchButton;


@end
