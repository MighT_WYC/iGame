//
//  IGFindCommentView.h
//  iGameApp
//
//  Created by lanou on 16/1/13.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGFindCommentView : UIView

@property (nonatomic, strong) UITextField *commentTextFiled;
@property (nonatomic, strong) UIButton *commentCountButton;
@property (nonatomic, strong) UIView *writeCommentView;
@property (nonatomic, strong) UITextView *writeCommentTV;
@property (nonatomic, strong) UIButton *cancerButton;
@property (nonatomic, strong) UIButton *sendButton;
@property (nonatomic, strong) UIButton *commentIconButton;

@end
