//
//  IGAFNetworking.h
//  iGameApp
//
//  Created by Yf66 on 16/1/11.
//  Copyright © 2016年 WYC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
typedef void (^Finish)(NSDictionary *dataDic);
typedef void (^Error)(NSError *requestError);
typedef NS_ENUM(NSInteger, RequestType) {
    GET,
    POST
};

@interface IGAFNetworking : NSObject

//  数据请求

+ (void)requestWithUrlString:(NSString *)urlString parDic:(NSDictionary *)parDic metho:(RequestType )method finish:(Finish)finish error:(Error)requestError;

+ (void)newRequestWithUrlString:(NSString *)urlString parDic:(NSDictionary *)parDic method:(RequestType)method finish:(Finish)finish error:(Error)error;

@end
